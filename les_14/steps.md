### Patroni

#### 1. Создаем 3 ВМ.

Создаем 3 ВМ:
- psql_1
- psql_2
- etcd

#### 2. Настраиваем etcd хост

Ставим сам etcd
```
apt-get install etcd -y
```

Проверяем:
```
hound672@etcd:~$ sudo systemctl status etcd
● etcd.service - etcd - highly-available key value store
     Loaded: loaded (/lib/systemd/system/etcd.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2022-01-13 18:54:52 UTC; 14s ago
       Docs: https://github.com/coreos/etcd
             man:etcd
   Main PID: 2266 (etcd)
      Tasks: 11 (limit: 1159)
     Memory: 3.4M
     CGroup: /system.slice/etcd.service
             └─2266 /usr/bin/etcd

Jan 13 18:54:52 etcd etcd[2266]: 8e9e05c52164694d received MsgVoteResp from 8e9e05c52164694d at term 2
Jan 13 18:54:52 etcd etcd[2266]: 8e9e05c52164694d became leader at term 2
Jan 13 18:54:52 etcd etcd[2266]: raft.node: 8e9e05c52164694d elected leader 8e9e05c52164694d at term 2
Jan 13 18:54:52 etcd etcd[2266]: setting up the initial cluster version to 3.2
Jan 13 18:54:52 etcd etcd[2266]: set the initial cluster version to 3.2
Jan 13 18:54:52 etcd etcd[2266]: enabled capabilities for version 3.2
Jan 13 18:54:52 etcd etcd[2266]: published {Name:etcd ClientURLs:[http://localhost:2379]} to cluster cdf818194e3a8c32
Jan 13 18:54:52 etcd etcd[2266]: ready to serve client requests
Jan 13 18:54:52 etcd systemd[1]: Started etcd - highly-available key value store.
Jan 13 18:54:52 etcd etcd[2266]: serving insecure client requests on 127.0.0.1:2379, this is strongly discouraged!
```

Внутрений айпи etcd: 10.128.0.16

На всякий пингуем с тачек с погрей тачку с etcd - все ок.

А вот телнетом достучать до порта не получилось. Идем в конфиг etcd и добавляет туда.
Перезагружаем сервис etcd, проверяем снова доступ телнетом - все ок.

```
ETCD_LISTEN_PEER_URLS="http://10.128.0.16:2380,http://127.0.0.1:7001"
ETCD_LISTEN_CLIENT_URLS="http://127.0.0.1:2379, http://10.128.0.16:2379"
ETCD_INITIAL_ADVERTISE_PEER_URLS="http://10.128.0.16:2380"
ETCD_INITIAL_CLUSTER="etcd0=http://10.128.0.16:2380,"
ETCD_ADVERTISE_CLIENT_URLS="http://10.128.0.16:2379"
ETCD_INITIAL_CLUSTER_TOKEN="cluster1"
ETCD_INITIAL_CLUSTER_STATE="new"
```

#### 3. Настраиваем psql хосты

Ставим postgres 14.

Ставим pip:
```
sudo apt install python3-pip
```

Ставим патрони:
```
sudo pip install patroni[etcd]
```

Доставляем драйвер для посгри:
```
sudo pip install psycopg
```

Настраиваем patroni.

```
sudo mkdir -p /etc/patroni
cd /etc/patroni
```

Берем заготовку конфига с гита патрони:
```
sudo wget https://raw.githubusercontent.com/zalando/patroni/master/postgres0.yml
```

Начинаем его менять.

1. Указываем адрес etcd:
```
etcd:
  #Provide host to do the initial discovery of the cluster topology:
  host: 10.128.0.16:2379
```

#### 4. Пробуем запустить все

Стопаем посгрес:
```
sudo systemctl stop postgresql
```

Пытаемся запустить на первой ВМ.
```
sudo -u postgres patroni postgres0.yml
```

Но получаем ошибку:
```
FileNotFoundError: [Errno 2] No such file or directory: 'pg_ctl'
```

Гуглим...
оказывается нужно указать путь до бинарей в конфиге патрони. Прописываем:
```
bin_dir: /usr/lib/postgresql/14/bin
```

Запускаем...снова ошибка:
```
creating directory data/postgresql0 ... initdb: error: could not create directory "data": Permission denied
```

не создал и не указал дирректорю для патрони. Делаем:
```
sudo mkdir -p /var/lib/postgresql/patroni
sudo chown postgres:postgres /var/lib/postgresql/patroni
```

И снова запускаем...

```
2022-01-13 19:20:05,699 INFO: No PostgreSQL configuration items changed, nothing to reload.
2022-01-13 19:20:05,704 INFO: Lock owner: None; I am postgresql0
2022-01-13 19:20:05,707 INFO: trying to bootstrap a new cluster
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locale "C.UTF-8".
The default text search configuration will be set to "english".

Data page checksums are enabled.

fixing permissions on existing directory /var/lib/postgresql/patroni ... ok
creating subdirectories ... ok
selecting dynamic shared memory implementation ... posix
selecting default max_connections ... 100
selecting default shared_buffers ... 128MB
selecting default time zone ... Etc/UTC
creating configuration files ... ok
running bootstrap script ... ok
performing post-bootstrap initialization ... ok
syncing data to disk ... ok

initdb: warning: enabling "trust" authentication for local connections
You can change this by editing pg_hba.conf or using the option -A, or
--auth-local and --auth-host, the next time you run initdb.

Success. You can now start the database server using:

    /usr/lib/postgresql/14/bin/pg_ctl -D /var/lib/postgresql/patroni -l logfile start

2022-01-13 19:20:07.743 UTC [17899] LOG:  starting PostgreSQL 14.1 (Ubuntu 14.1-2.pgdg20.04+1) on x86_64-pc-linux-gnu, compiled by gcc (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0, 64-bit
2022-01-13 19:20:07.744 UTC [17899] LOG:  listening on IPv4 address "127.0.0.1", port 5432
2022-01-13 19:20:07.748 UTC [17899] LOG:  listening on Unix socket "./.s.PGSQL.5432"
2022-01-13 19:20:07.756 UTC [17900] LOG:  database system was shut down at 2022-01-13 19:20:06 UTC
2022-01-13 19:20:07.767 UTC [17899] LOG:  database system is ready to accept connections
2022-01-13 19:20:07,785 INFO: postmaster pid=17899
localhost:5432 - accepting connections
localhost:5432 - accepting connections
2022-01-13 19:20:07,838 INFO: establishing a new patroni connection to the postgres cluster
2022-01-13 19:20:07,851 INFO: running post_bootstrap
2022-01-13 19:20:07,901 WARNING: Could not activate Linux watchdog device: "Can't open watchdog device: [Errno 2] No such file or directory: '/dev/watchdog'"
2022-01-13 19:20:07,924 INFO: initialized a new cluster
2022-01-13 19:20:17,915 INFO: no action. I am (postgresql0), the leader with the lock

```

It's alive???

Проверим...
попробуем глянуть, что тулза патрони скажет, в новой сессии запускаем:
```
hound672@psql1:/etc/patroni$ patronictl -c postgres0.yml list batman
+-------------+-----------+--------+---------+----+-----------+
| Member      | Host      | Role   | State   | TL | Lag in MB |
+ Cluster: batman (7052772693646255586) -----+----+-----------+
| postgresql0 | 127.0.0.1 | Leader | running |  1 |           |
+-------------+-----------+--------+---------+----+-----------+
```

Через psql тоже все заходит.

Идем на psql2 и запускаем патрони там.

```
hound672@psql2:/etc/patroni$ sudo -u postgres patroni postgres0.yml
2022-01-13 19:27:21,225 INFO: No PostgreSQL configuration items changed, nothing to reload.
2022-01-13 19:27:21,231 INFO: Lock owner: postgresql0; I am postgresql1
2022-01-13 19:27:21,233 INFO: trying to bootstrap from leader 'postgresql0'
pg_basebackup: error: connection to server at "127.0.0.1", port 5432 failed: Connection refused
        Is the server running on that host and accepting TCP/IP connections?
2022-01-13 19:27:21,247 ERROR: Error when fetching backup: pg_basebackup exited with code=1
2022-01-13 19:27:21,247 WARNING: Trying again in 5 seconds
pg_basebackup: error: connection to server at "127.0.0.1", port 5432 failed: Connection refused
        Is the server running on that host and accepting TCP/IP connections?
```

Ошибка...
Порт из вне доступен, но в конфиге патрони мы не поменяли шаблонизацию для pg_hba.

Пока разрешим всем хостам для реплики подключаться:
```
- host replication replicator 0.0.0.0/0 md5
```

Снова не работает. Читаем доку (сразу то не нельзя было)
```
Мы должны определить listen_address и connect_address. Listen_address – это где Postgres будет слушать. И Connect_address – это для того, чтобы в etcd опубликовать, как к нам можно обратиться.
```

Прописываем свой адрес, перезапускаем...и все заработало.

```
hound672@psql1:/etc/patroni$ patronictl -c postgres0.yml list batman
+-------------+-------------+---------+---------+----+-----------+
| Member      | Host        | Role    | State   | TL | Lag in MB |
+ Cluster: batman (7052772693646255586) --------+----+-----------+
| postgresql0 | 10.128.0.17 | Leader  | running | 10 |           |
| postgresql1 | 10.128.0.18 | Replica | running | 10 |         0 |
+-------------+-------------+---------+---------+----+-----------+
```

Создаем базу на psql1, на psql2 отображается. Таблица тоже.
Вроде как все работает.

Попробуем что-то вставить в psql2.
```
test=# create table test2(id serial);
ERROR:  cannot execute CREATE TABLE in a read-only transaction
```

Справедливо получили ошибку.

А теперь попробуем грохнуть мастера и посмотрим, что получится.
Уводим в ребут тачку (а т.к у нас запуск патрони не через systemd и вообще не демонизирован, то автостарта не будет)
```
sudo reboot
```

После того как первая тачка отключилась:
```
2022-01-13 19:51:27,487 INFO: no action. I am (postgresql1), a secondary, and following a leader (postgresql0)
2022-01-13 19:51:33.720 UTC [18121] FATAL:  could not receive data from WAL stream: FATAL:  terminating connection due to administrator command
2022-01-13 19:51:33.720 UTC [18107] LOG:  invalid record length at 0/30256F0: wanted 24, got 0
2022-01-13 19:51:33.734 UTC [18276] FATAL:  could not connect to the primary server: connection to server at "10.128.0.17", port 5432 failed: FATAL:  the database system is shutting down
2022-01-13 19:51:36,341 INFO: Got response from postgresql0 http://127.0.0.1:8008/patroni: {"state": "running", "postmaster_start_time": "2022-01-13 19:44:33.544039+00:00", "role": "replica", "server_version": 140001, "xlog": {"received_location": 50484976, "replayed_location": 50484976, "replayed_timestamp": "2022-01-13 19:47:43.920895+00:00", "paused": false}, "timeline": 10, "cluster_unlocked": true, "dcs_last_seen": 1642103496, "database_system_identifier": "7052772693646255586", "patroni": {"version": "2.1.2", "scope": "batman"}}
2022-01-13 19:51:36,348 WARNING: Could not activate Linux watchdog device: "Can't open watchdog device: [Errno 2] No such file or directory: '/dev/watchdog'"
2022-01-13 19:51:36,351 INFO: promoted self to leader by acquiring session lock
2022-01-13 19:51:36.359 UTC [18107] LOG:  received promote request
server promoting
2022-01-13 19:51:36.359 UTC [18107] LOG:  redo done at 0/30256B8 system usage: CPU: user: 0.00 s, system: 0.03 s, elapsed: 422.75 s
2022-01-13 19:51:36.359 UTC [18107] LOG:  last completed transaction was at log time 2022-01-13 19:47:43.920895+00
2022-01-13 19:51:36,359 INFO: cleared rewind state after becoming the leader
2022-01-13 19:51:36.364 UTC [18107] LOG:  selected new timeline ID: 11
2022-01-13 19:51:36.709 UTC [18107] LOG:  archive recovery complete
2022-01-13 19:51:36.725 UTC [18105] LOG:  database system is ready to accept connections
2022-01-13 19:51:37,414 INFO: no action. I am (postgresql1), the leader with the lock
```

Смотрим статус через тулзу:
```
hound672@psql2:/etc/patroni$ patronictl -c postgres0.yml list batman
+-------------+-------------+--------+---------+----+-----------+
| Member      | Host        | Role   | State   | TL | Lag in MB |
+ Cluster: batman (7052772693646255586) -------+----+-----------+
| postgresql1 | 10.128.0.18 | Leader | running | 11 |           |
+-------------+-------------+--------+---------+----+-----------+
```

Теперь вторая тачка лидер/мастер (мастер сейчас нельзя вообще говорить, но как-то пофик).

Данные тоже на месте.

Поднимем первую тачку.
```
hound672@psql2:/etc/patroni$ patronictl -c postgres0.yml list batman
+-------------+-------------+---------+---------+----+-----------+
| Member      | Host        | Role    | State   | TL | Lag in MB |
+ Cluster: batman (7052772693646255586) --------+----+-----------+
| postgresql0 | 10.128.0.17 | Replica | running | 11 |         0 |
| postgresql1 | 10.128.0.18 | Leader  | running | 11 |           |
+-------------+-------------+---------+---------+----+-----------+
```

Теперь поменялись местами, на первой не могу ничего создавать, на второй могу.
И изменения подтягиваются на первую тачку.

ВОПРОС:
1. Пишут, что одного инстанса etcd не достаточно, но тут я не совсем понял. Нужно несколько ВМ разворачивать с etcd?


#### 4. Смотрим вэб морду патрони

Мне лень идти на вэб консоль гугла чтобы открыть порт на внешку, поэтому буду ssh тунель использовать.

```
ssh -L 8008:localhost:8008 gce-psql1
```

```gce-psql1``` - алиас для хоста, который прописан в ~/.ssh/config.

Данные для графаны видно, отлично.
```
# HELP patroni_version Patroni semver without periods.
# TYPE patroni_version gauge
patroni_version{scope="batman"} 020102
# HELP patroni_postgres_running Value is 1 if Postgres is running, 0 otherwise.
# TYPE patroni_postgres_running gauge
patroni_postgres_running{scope="batman"} 1
# HELP patroni_postmaster_start_time Epoch seconds since Postgres started.
# TYPE patroni_postmaster_start_time gauge
patroni_postmaster_start_time{scope="batman"} 1642139990.105568
# HELP patroni_master Value is 1 if this node is the leader, 0 otherwise.
# TYPE patroni_master gauge
patroni_master{scope="batman"} 1
# HELP patroni_xlog_location Current location of the Postgres transaction log, 0 if this node is not the leader.
# TYPE patroni_xlog_location counter
patroni_xlog_location{scope="batman"} 50599320
# HELP patroni_standby_leader Value is 1 if this node is the standby_leader, 0 otherwise.
# TYPE patroni_standby_leader gauge
patroni_standby_leader{scope="batman"} 0
# HELP patroni_replica Value is 1 if this node is a replica, 0 otherwise.
# TYPE patroni_replica gauge
patroni_replica{scope="batman"} 0
# HELP patroni_xlog_received_location Current location of the received Postgres transaction log, 0 if this node is not a replica.
# TYPE patroni_xlog_received_location counter
patroni_xlog_received_location{scope="batman"} 0
# HELP patroni_xlog_replayed_location Current location of the replayed Postgres transaction log, 0 if this node is not a replica.
# TYPE patroni_xlog_replayed_location counter
patroni_xlog_replayed_location{scope="batman"} 0
# HELP patroni_xlog_replayed_timestamp Current timestamp of the replayed Postgres transaction log, 0 if null.
# TYPE patroni_xlog_replayed_timestamp gauge
patroni_xlog_replayed_timestamp{scope="batman"} 0
# HELP patroni_xlog_paused Value is 1 if the Postgres xlog is paused, 0 otherwise.
# TYPE patroni_xlog_paused gauge
patroni_xlog_paused{scope="batman"} 0
# HELP patroni_postgres_server_version Version of Postgres (if running), 0 otherwise.
# TYPE patroni_postgres_server_version gauge
patroni_postgres_server_version {scope="batman"} 140001
# HELP patroni_cluster_unlocked Value is 1 if the cluster is unlocked, 0 if locked.
# TYPE patroni_cluster_unlocked gauge
patroni_cluster_unlocked{scope="batman"} 0
# HELP patroni_postgres_timeline Postgres timeline of this node (if running), 0 otherwise.
# TYPE patroni_postgres_timeline counter
patroni_postgres_timeline{scope="batman"} 12
# HELP patroni_dcs_last_seen Epoch timestamp when DCS was last contacted successfully by Patroni.
# TYPE patroni_dcs_last_seen gauge
patroni_dcs_last_seen{scope="batman"} 1642140122
```

#### 5. Новый хост

Создадим еще одну ВМ, но в другом регионе. (В финке, может хоть пинг меньше будет)

Ставим на него посгри.

Пока ставится посгри пробуем пингануть новый хост из других хостов по внутренему айпи и проверить порты.

например, с psql2 делаем:
```
hound672@psql2:~$ ping 10.166.0.2
PING 10.166.0.2 (10.166.0.2) 56(84) bytes of data.
64 bytes from 10.166.0.2: icmp_seq=1 ttl=64 time=127 ms
64 bytes from 10.166.0.2: icmp_seq=2 ttl=64 time=125 ms
```

Пинг проходит, теперь проверим порты.
Заходим на psql3 и вводим:
```
hound672@psql3:~$ nc -lp 5555
```

Тем самым мы открыли тсп порт 5555 и слушаем его. На psql2 используя тот же неткат или телнет пробуем открыть этот порт:
```
hound672@psql2:~$ telnet 10.166.0.2 5555
Trying 10.166.0.2...
Connected to 10.166.0.2.
Escape character is '^]'.
```

Порт открылся, данные прошли, значит можно продолжать настраивать дальше.`

Ставим патрони, настраиваем, как делали это раньше.

Все поставили, конфиг сделали, запускаем.

```
sudo -u postgres patroni postgres2.yml
```

Все запустилось.
Смотрим статус:
```
hound672@psql1:/etc/patroni$ patronictl -c postgres0.yml list batman
+-------------+-------------+---------+---------+----+-----------+
| Member      | Host        | Role    | State   | TL | Lag in MB |
+ Cluster: batman (7052772693646255586) --------+----+-----------+
| postgresql0 | 10.128.0.17 | Replica | running | 13 |         0 |
| postgresql1 | 10.128.0.18 | Leader  | running | 13 |           |
| postgresql2 | 10.166.0.2  | Replica | running | 13 |         0 |
+-------------+-------------+---------+---------+----+-----------+
```

Теперь у нас кластер из 3-ех ВМ.

ВОПРОСЫ: 
1. Вот тут я не совсем понимаю как работать с бекапами?
С какой машины их снимать?
Я использую pgbackrest, сейчас на раб. проекте просто снимаю с реплики, но там у меня не патрони, руками настраивал репликацию (надо будет переехать на патрони, наверное).
А тут у нас несколько реплик с какой снимать и как потом делать восстановление?
Например, что-то накосячили в запросе, надо делать откат, берем бекап на какой-то из реплик, стопаем весь кластер, делаем восстановление из этого кластера и делаем эту реплику мастером?

Заметил ошибки в логах на новой ВМ:
```
2022-01-14 06:27:57.956 UTC [18144] FATAL:  could not receive data from WAL stream: ERROR:  requested starting point 0/6000000 is ahead of the WAL flush position of this server 0/5000148
2022-01-14 06:27:59,482 INFO: no action. I am (postgresql2), a secondary, and following a leader (postgresql1)
2022-01-14 06:28:02.961 UTC [18145] LOG:  started streaming WAL from primary at 0/6000000 on timeline 13
2022-01-14 06:28:02.961 UTC [18145] FATAL:  could not receive data from WAL stream: ERROR:  requested starting point 0/6000000 is ahead of the WAL flush position of this server 0/5000148
```

Хотя если заходить через psql и смотреть содержимое базы, то там все, все данные на месте.
Попробовал на мастере добавить что-то в таблицу, на старой реплики изменения подтянулись, на новой нет.

Удалил данные патрони и запустил его по новой, все заработало, ошибок нет.
Видимо это было из-за того, что я сначала скопировал конфиг с psql1 и не поменял там имя хоста и из-за этого могли быть какие-то коллизии.
Сейчас данные в новой реплике успешно обновляются. 

#### 6. Точка входа

HA кластер у нас вроде как настроен.
Но точек входа у нас потенциально 3 (мастер и 2 реплики), при этом если мастер упадет, то приложениям надо говорить, что адрес подключения поменялся.
Не очень удобно. В инете много статей с примерами для HAProxy, но я подумал попробовать pgbouncer.

Создаю отдельную тачку для него на гугле.

Начинаем настраивать:

```
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - && sudo apt-get update
```

```
sudo apt install pgbouncer -y
```

Поставили, в конфиге прописали айпи адрес мастера, пробуем зайти.
```
hound672@psql3:/var/lib/postgresql$ psql -h 10.166.0.3 -p 6432 -d postgres -U postgres
Password for user postgres:
psql (14.1 (Ubuntu 14.1-2.pgdg20.04+1))
Type "help" for help.

postgres=# \d
Did not find any relations.
postgres=# \c test
You are now connected to database "test" as user "postgres".
test=# \d
              List of relations
 Schema |     Name     |   Type   |  Owner
--------+--------------+----------+----------
 public | test1        | table    | postgres
 public | test1_id_seq | sequence | postgres
 public | ttt          | table    | postgres
 public | ttt_id_seq   | sequence | postgres
(4 rows)
```

тут у меня была проблема с паролем, долго не мог зайти, в итоге в pgbouncer.
Пока сделал таким образом:
1. Создал файл ```/etc/pgbouncer/userlist.txt```
2. В нем прописал: 
```
sudo cat userlist.txt
"postgres" "12346"
```
И поставил пароль на мастер ноде для юзера postgres.

Теперь пробую подключится с локальной машины:
1. Делаю тунель:
```
ssh -L 6432:localhost:6432 gce-loader
```

2. С локальной машины запускаю psql:
```
psql -h localhost -p 6432 -d postgres -U postgres                                                                                                                  2 ✘
Password for user postgres:
psql (13.4, server 14.1 (Ubuntu 14.1-2.pgdg20.04+1))
WARNING: psql major version 13, server major version 14.
         Some psql features might not work.
Type "help" for help.

postgres=# \c test
psql (13.4, server 14.1 (Ubuntu 14.1-2.pgdg20.04+1))
WARNING: psql major version 13, server major version 14.
         Some psql features might not work.
You are now connected to database "test" as user "postgres".
test=# select * from test1;
 id
----
  1
  2
  3
(3 rows)
```

Данные могу теперь получать.

ВОПРОСЫ:
1. На сколько такой подход адекватен. если сейчас его использовать не проде?
Какие недочеты я допустил?
2. Пока не получилось сделать шаблонизацию конфига, чтобы pgbouncer автоматически переключался на новый мастер.
