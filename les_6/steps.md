#### 1. Настройте выполнение контрольной точки раз в 30 секунд.

Создал тестовую БД
```
CREATE DATABASE checkpoints_test;
```

Поменял настройки выполнения контрольной точки:
```
ALTER SYSTEM SET checkpoint_timeout TO '30s';
```

После чего обновил конфиг посгреса командой:
```
SELECT pg_reload_conf();
```

Проверил, что настройка для контольной точки прмиенилась:
```
show checkpoint_timeout;
30s
```

Запомнил текущий объем wal файлов:
```
cd /var/lib/postgresql/14/main/pg_wal
du -sh .
129MB
131080
```

Хотел сначала так сделать, сравнить фактический объем на файловой системе,
но потом вспомнил, что посгря, вроде как, создает эти файлы и заполняет нулями, а потом уже по мере
накопления данных вместо нулей пишет данные.

И сохранил статистику:
```
postgres=# select * from pg_stat_bgwriter \gx
-[ RECORD 1 ]---------+----------------------------
checkpoints_timed     | 99
checkpoints_req       | 14
checkpoint_write_time | 9656354
checkpoint_sync_time  | 2146
buffers_checkpoint    | 134767
buffers_clean         | 0
maxwritten_clean      | 0
buffers_backend       | 25718
buffers_backend_fsync | 0
buffers_alloc         | 33463
stats_reset           | 2021-10-21 18:54:52.4374+00

postgres=#  SELECT pg_current_wal_insert_lsn();
 pg_current_wal_insert_lsn 
---------------------------
 0/98D959E0
```

На данный момент wal занимают 129МБ.

#### 2. 10 минут c помощью утилиты pgbench подавайте нагрузку.

Подготовил базу к нагрузочным тестам:
```
pgbench -i checkpoints_test
```

Запустил бенч:
```
pgbench -P 10 -T 600 -U postgres checkpoints_test
```

#### 3. Измерьте, какой объем журнальных файлов был сгенерирован за это время. Оцените, какой объем приходится в среднем на одну контрольную точку.

```
postgres=#  SELECT pg_current_wal_insert_lsn();
 pg_current_wal_insert_lsn 
---------------------------
 0/AE3F3368
(1 row)

postgres=# select * from pg_stat_bgwriter \gx
-[ RECORD 1 ]---------+----------------------------
checkpoints_timed     | 119
checkpoints_req       | 14
checkpoint_write_time | 10166931
checkpoint_sync_time  | 2843
buffers_checkpoint    | 167489
buffers_clean         | 0
maxwritten_clean      | 0
buffers_backend       | 26903
buffers_backend_fsync | 0
buffers_alloc         | 36746
stats_reset           | 2021-10-21 18:54:52.4374+00
```

Для измерения объема журнальных файлов возьмем позицию после бенча и вычтем из нее позицию, которая была до бенча
```
0xAE3F3368 - 0x98D959E0 = 0x1565D988 = 358996360
```

Размер журнальных файлов составил 358996360 Байт

#### 4. Проверьте данные статистики: все ли контрольные точки выполнялись точно по расписанию. Почему так произошло?

Согласно статистики полученной с помощью представления ```pg_stat_bgwriter```
за время бенча было выполнено 20 контрольных точек:
```
разница значений checkpoints_timed после и дло теста.
```

Ранее мы поставили таймаут в 30 сек, тест длился 10мин (600 сек)
600 / 30 = 20, т.е 20 контрольных точек мы и ожидали.

#### 5. Сравните tps в синхронном/асинхронном режиме утилитой pgbench. Объясните полученный результат.

Проверяем сначала, что включен синхронный режим:
```
postgres=# show synchronous_commit;
 synchronous_commit
--------------------
 on
(1 row)
```

И запускам бенч командой:
```
pgbench -P 1 -T 60 -U postgres checkpoints_test
```

Полученные результаты:
```
pgbench (14.0 (Ubuntu 14.0-1.pgdg20.04+1))
starting vacuum...end.
progress: 1.0 s, 719.9 tps, lat 1.380 ms stddev 0.193
progress: 2.0 s, 621.0 tps, lat 1.610 ms stddev 0.230
progress: 3.0 s, 499.0 tps, lat 1.978 ms stddev 0.636
progress: 4.0 s, 289.0 tps, lat 3.474 ms stddev 1.481
progress: 5.0 s, 288.0 tps, lat 3.477 ms stddev 1.413
progress: 6.0 s, 290.0 tps, lat 3.453 ms stddev 1.563
progress: 7.0 s, 290.0 tps, lat 3.462 ms stddev 1.626
progress: 8.0 s, 285.0 tps, lat 3.507 ms stddev 2.177
progress: 9.0 s, 283.0 tps, lat 3.540 ms stddev 2.291
progress: 10.0 s, 277.0 tps, lat 3.604 ms stddev 3.047
transaction type: <builtin: TPC-B (sort of)>
scaling factor: 1
query mode: simple
number of clients: 1
number of threads: 1
duration: 10 s
number of transactions actually processed: 3843
latency average = 2.601 ms
latency stddev = 1.757 ms
initial connection time = 4.777 ms
tps = 384.419362 (without initial connection time)
```

После чего включаем ассинхронный режим:

```
postgres=# ALTER SYSTEM SET synchronous_commit = off;
ALTER SYSTEM
```

И обновляем конфигурацию посгреса:
```
SELECT pg_reload_conf();
```

И проверяем, что обновления конфигурации подтянулись:

```
postgres=# show synchronous_commit;
 synchronous_commit
--------------------
 off
(1 row)
```

Снова запускаем бенч:
```
pgbench -P 1 -T 10 -U postgres checkpoints_test
```

Получаем уже другие результаты:
```
pgbench (14.0 (Ubuntu 14.0-1.pgdg20.04+1))
starting vacuum...end.
progress: 1.0 s, 1222.9 tps, lat 0.813 ms stddev 0.161
progress: 2.0 s, 1259.9 tps, lat 0.793 ms stddev 0.132
progress: 3.0 s, 1273.0 tps, lat 0.786 ms stddev 0.126
progress: 4.0 s, 1299.1 tps, lat 0.769 ms stddev 0.111
progress: 5.0 s, 1306.1 tps, lat 0.766 ms stddev 0.109
progress: 6.0 s, 1324.0 tps, lat 0.755 ms stddev 0.107
progress: 7.0 s, 1331.0 tps, lat 0.751 ms stddev 0.108
progress: 8.0 s, 1330.9 tps, lat 0.751 ms stddev 0.103
progress: 9.0 s, 1331.1 tps, lat 0.751 ms stddev 0.139
progress: 10.0 s, 1299.0 tps, lat 0.769 ms stddev 0.119
transaction type: <builtin: TPC-B (sort of)>
scaling factor: 1
query mode: simple
number of clients: 1
number of threads: 1
duration: 10 s
number of transactions actually processed: 12978
latency average = 0.770 ms
latency stddev = 0.124 ms
initial connection time = 4.720 ms
tps = 1298.350111 (without initial connection time)
```

Как видно tps сильно вырос. И сильно уменьшилась задержка 0,770 против 2,601

Это следствие того, что при синхронном режиме мы записываем накопившиеся данные по мере их накопления,
а в синхронном дожидаеся полного заполнения страницы.
Из-за чего в синхронном режиме у нас получается больше операций ввода/вывода,
а при ассинхроном операций ввода/вывода меньше, но и появляется риск потери данных при восстановлении после сбоя.
Т.к часть данных может просто не записаться на диск, из-за того, что страница была не заполнена полностью


#### 6. Создайте новый кластер с включенной контрольной суммой страниц. Создайте таблицу. Вставьте несколько значений. Выключите кластер. Измените пару байт в таблице. Включите кластер и сделайте выборку из таблицы. Что и почему произошло? как проигнорировать ошибку и продолжить работу?

Создал кластер в включенной контрольной суммой страниц:
```
pg_createcluster 14 main_crc -- --data-checksums
```

Подключился к нему с помощью psql
```
psql -p 5434
```

Проверил, что подключилсь к новому кластеру:
```
postgres=# SELECT current_setting('cluster_name');
 current_setting 
-----------------
 14/main_crc
(1 row)
```

Создал таблицу и вставил несколько значений:
```
postgres=# CREATE TABLE test_table (c int);
CREATE TABLE

postgres=# INSERT INTO test_table (c) VALUES (1), (2), (3);
INSERT 0 3

postgres=# SELECT * FROM test_table;
 c 
---
 1
 2
 3
```

Проверил, что проверка чек суммы активна:
```
postgres=# show data_checksums;
 data_checksums 
----------------
 on
(1 row)
```

И проверил что игнорирование ошибок чек суммы выключено:
```
postgres=# show ignore_checksum_failure;
 ignore_checksum_failure 
-------------------------
 off
```

Получил путь к файлу на файловой системе в котором находится тестовая таблица:
```
postgres=# SELECT pg_relation_filepath('test_table');
 pg_relation_filepath
----------------------
 base/13724/16384
```

Остановил кластер:
```
$ pg_ctlcluster 14 main_crc stop
$ pg_lsclusters
Ver Cluster  Port Status Owner    Data directory                  Log file
14  main     5432 online postgres /var/lib/postgresql/14/main     /var/log/postgresql/postgresql-14-main.log
14  main_crc 5434 down   postgres /var/lib/postgresql/14/main_crc /var/log/postgresql/postgresql-14-main_crc.log
```

Сделал бекап файла с таблицей:
```
cp 16384 /home/hound672
```

Отредактировал пару байт в файле 16384.

Запустил кластер:
```
$ pg_ctlcluster 14 main_crc start
$ pg_lsclusters
Ver Cluster  Port Status Owner    Data directory                  Log file
14  main     5432 online postgres /var/lib/postgresql/14/main     /var/log/postgresql/postgresql-14-main.log
14  main_crc 5434 online   postgres /var/lib/postgresql/14/main_crc /var/log/postgresql/postgresql-14-main_crc.log
```

Подключился к нему с помощью psql
```
psql -p 5434
```

Попробовал прочитать данные из таблицы и получил ошибку:
```
postgres=# select * from test_table;
WARNING:  page verification failed, calculated checksum 37405 but expected 26124
ERROR:  invalid page in block 0 of relation base/13724/16384
```

Ошибка справедливая, т.к мы принудтельно поменяли в начале файла таблицы пару байт, тем самым нарушив целостоность.

Отключил проверку чек суммы:
```
postgres=# set ignore_checksum_failure = on;
SET
postgres=# show ignore_checksum_failure;
 ignore_checksum_failure
-------------------------
 on
(1 row)
```

И выполнил запрос чтения повторно:
```
postgres=# select * from test_table;
WARNING:  page verification failed, calculated checksum 37405 but expected 26124
 c
---
 1
 2
 3
(3 rows)
```

Ранее записанные прочитаны, но появилось предупреждение о нарушение чек суммы (что логично).

??? Возможно ли восстановление данных с помощью wal файлов?
Журнал проводит запись всех изменений данных, которые были сделаны с помощью СУБД,
соотвественно вся история у нас есть в журнале, возможно "проиграв" который мы могли бы восстановить данные 
без игнорирования ошибки чек суммы.
