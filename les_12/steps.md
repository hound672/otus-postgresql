### Секционирование таблицы

#### 1. Настройка окружения

Развернул ВМ, установил СУБД.
Все как и раньше.

#### 2. Наполнение данными.

Скачал дамп таблицы flights.
Залил его в ранее созданую БД с помощью ```psql```.

#### 3. Секционирование.

Решил секционировать таблицу ```bookings``` по полю ```book_date```.

Сначала создам таблицу ```bookings_parts```

```
create table bookings_parts
(
    book_ref     char(6)                  not null,
    book_date    timestamp with time zone not null,
    total_amount numeric(10, 2)           not null
) partition by range (book_date)
```

И создаем секции:

```
create table bookings_parts_2016 partition of bookings_parts for values from ('2016-01-01') to ('2017-01-01')
create table bookings_parts_2017 partition of bookings_parts for values from ('2017-01-01') to ('2018-01-01')
```

#### 4. Заполняем данными

Берем исходную таблцу и заполняем новую таблицу.

```
insert into bookings_parts (select * from bookings)
```

#### 5. Проверка

Возьмем простой запрос:

```
explain select * from bookings_parts where book_date = '2017-09-21 15:41:00.000000 +00:00'
```

План запроса:
```
Gather  (cost=1000.00..15567.49 rows=5 width=21)
  Workers Planned: 2
  ->  Parallel Seq Scan on bookings_parts_2017 bookings_parts  (cost=0.00..14566.99 rows=2 width=21)
        Filter: (book_date = '2017-09-21 15:41:00+00'::timestamp with time zone)
```

Согласно плану мы сразу перешли к поиску в партиции ```bookings_parts_2017```.
