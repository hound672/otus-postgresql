#### 1. Настройте сервер так, чтобы в журнал сообщений сбрасывалась информация о блокировках, удерживаемых более 200 миллисекунд. Воспроизведите ситуацию, при которой в журнале появятся такие сообщения.

- Создаем тестовую таблицу
```
CREATE TABLE test_lock (id serial, value int);
```

- Вставляем записи
```
INSERT INTO test_lock (id, value) VALUES (1, 111), (2, 222), (3,333);
```

- Включаем логирование сообщений об ожидании блокировок
```
ALTER SYSTEM SET log_lock_waits = on;
ALTER SYSTEM SET deadlock_timeout='200ms';
SELECT pg_reload_conf();
```

- Проверим, что настройки применились
```
test_db=# SHOW log_lock_waits;
 log_lock_waits 
----------------
 on
(1 row)

test_db=# SHOW deadlock_timeout;
 deadlock_timeout 
------------------
 200ms
(1 row)
```

- Дальше работаем в разных сессиях (разные сеансы в терминале)

SESSION_1:
```
BEGIN;
UPDATE test_lock SET value = 1 WHERE id = 1;
UPDATE 1
```
Тут мы не заблокировались.

SESSION_2:
```
BEGIN;
UPDATE test_lock SET value = 11 WHERE id = 1;
```
А тут уже поймали блокировку.

- Идем в логи посгреса.
```
2021-10-28 21:07:37.060 UTC [6941] LOG:  process 6941 still waiting for ShareLock on transaction 832 after 200.053 ms
2021-10-28 21:07:37.060 UTC [6941] DETAIL:  Process holding the lock: 6927. Wait queue: 6941.
2021-10-28 21:07:37.060 UTC [6941] CONTEXT:  while updating tuple (0,7) in relation "test_lock"
2021-10-28 21:07:37.060 UTC [6941] STATEMENT:  UPDATE test_lock SET value = 1 WHERE id = 1;
```

Такое сообщение в логах мы получили после того как вторая транзакция (SESSION_2) прожала освобождение строки дольше 200мс.


#### 2. Смоделируйте ситуацию обновления одной и той же строки тремя командами UPDATE в разных сеансах. Изучите возникшие блокировки в представлении pg_locks и убедитесь, что все они понятны. Пришлите список блокировок и объясните, что значит каждая.

- Открываем 3 терминала, работаем с ранее созданной таблицей.

- Сначала в каждом терминале начнем транзакцию и узнаем pid текущей транзакции и процесса

SESSION_1
```
BEGIN;
SELECT txid_current(), pg_backend_pid();
 txid_current | pg_backend_pid 
--------------+----------------
          841 |           6927

```

SESSION_2
```
BEGIN;
SELECT txid_current(), pg_backend_pid();
 txid_current | pg_backend_pid 
--------------+----------------
          842 |           6941
```

SESSION_3
```
BEGIN;
SELECT txid_current(), pg_backend_pid();
 txid_current | pg_backend_pid 
--------------+----------------
          843 |           6984
```

- И также в каждом терминале пытаемся обновить одну и ту же строку.

SESSION_1
```
UPDATE test_lock SET value = 1 WHERE id = 1;
UPDATE 1
```

Транзакция не заблокировалась

SESSION_2
```
UPDATE test_lock SET value = 11 WHERE id = 1;
```

Тут мы заблокировались.

SESSION_3
```
UPDATE test_lock SET value = 111 WHERE id = 1;
```

И тут тоже заблокировались.

- Переходим к первому терминалу (потому что он остался один не заблокированным)
И выполеняем команду 
```
SELECT locktype, relation::REGCLASS, virtualxid, transactionid, mode, granted  FROM pg_locks WHERE pid = 6927;
   locktype    | relation  | virtualxid | transactionid |       mode       | granted 
---------------+-----------+------------+---------------+------------------+---------
 relation      | test_lock |            |               | RowExclusiveLock | t
 virtualxid    |           | 4/7538     |               | ExclusiveLock    | t
 transactionid |           |            |           841 | ExclusiveLock    | t
```
6927 - это pid процесса в котором мы запускали первую транхакцию (SESSION_1).

Первая строка говорит о том, что мы получили эксклюзивную блокировку на строку в таблице ```test_lock```.
Режим блокировки ```RowExclusiveLock```, что как раз соотвествуеет уровню блокировки оператора ```UPDATE```.

Выполним команду с указанием пида второго процесса:
```
SELECT locktype, relation::REGCLASS, virtualxid, transactionid, mode, granted  FROM pg_locks WHERE pid = 6941;
   locktype    | relation  | virtualxid | transactionid |       mode       | granted 
---------------+-----------+------------+---------------+------------------+---------
 relation      | test_lock |            |               | RowExclusiveLock | t
 virtualxid    |           | 5/2337     |               | ExclusiveLock    | t
 transactionid |           |            |           841 | ShareLock        | f
 transactionid |           |            |           842 | ExclusiveLock    | t
 tuple         | test_lock |            |               | ExclusiveLock    | t
```

Тут видим:
```
 relation      | test_lock |            |               | RowExclusiveLock | t
```
данную блокировку мы захватили, т.к режим ```RowExclusiveLock``` не конфликтует с самим собой, т.е в разных
транзакциях мы можем запрашивать такой режим блокирвки без блокирования транзакции (криво выразился правда :( )


``` transactionid |           |            |           841 | ShareLock        | f```
А вот уже данная строка говорит о том, что данная транзакция находится в ожидании завершения транзакции 841 - pid первой транзакции.
режим ```ShareLock``` уже конфликтует с режимом ```RowExclusiveLock```.
??? Тут правда не совсем мне понятно почему ```ShareLock``` тут блокировка...

И захваем тут блокировку с режимом ```ExclusiveLock``` на tuple:
```
 tuple         | test_lock |            |               | ExclusiveLock    | t
```


Выполним еще одну команду:
```
test_db=# SELECT pg_blocking_pids(6941);
 pg_blocking_pids 
------------------
 {6927}
(1 row)
```

6941 - pid второй транзакции, в ответе мы получили число ```6927```, что соответствует pid'у процесса в которой мы запустили первую транзакцию.

- Выполоним те же команды, но уже с указанием pid'а третьего процесса

```
test_db=# SELECT locktype, relation::REGCLASS, virtualxid, transactionid, mode, granted  FROM pg_locks WHERE pid = 6984;
   locktype    | relation  | virtualxid | transactionid |       mode       | granted 
---------------+-----------+------------+---------------+------------------+---------
 relation      | test_lock |            |               | RowExclusiveLock | t
 virtualxid    |           | 7/741      |               | ExclusiveLock    | t
 transactionid |           |            |           843 | ExclusiveLock    | t
 tuple         | test_lock |            |               | ExclusiveLock    | f
(4 rows)

test_db=# SELECT pg_blocking_pids(6984);
 pg_blocking_pids 
------------------
 {6941}
(1 row)
```

Строка 
```
 tuple         | test_lock |            |               | ExclusiveLock    | f
```
сообщает о том, что данная транзакция находится в ожидании блокировки, причем пытается получить доступ к tuple 
2-ой транзакции, а получить мы пытаемся с режимом ```ExclusiveLock```, который конфилктует с самим собой,
и следовательно текущая транзакция "засыпает".

Для того чтобы определить кто именно сейчас блокирует мы выполняем вторую команду, где получаем число ```6941```
которое соотвествует pid'у процесса второй транзакции.

- Далем commit в первой транзакции

- После чего вторая транзакция разблокируется

- Делаем коммит во второй транзкции, в результате чего разблокируем 3-ью транзакцию.


#### 3. Воспроизведите взаимоблокировку трех транзакций. Можно ли разобраться в ситуации постфактум, изучая журнал сообщений?

- Создаем таблицу.
```
CREATE TABLE dead_lock (id serial, value int);
```
- Вставляем в нее 3 записи:
```
INSERT INTO dead_lock (id, value) VALUES (1, 111), (2, 222), (3,333);
```
- Дальше работаем по сессиям (всегда в рамках транхакции)

SESSION_1:
```
BEGIN;
UPDATE dead_lock SET value = 11 WHERE id = 1;
```

SESSION_2:
```
BEGIN;
UPDATE dead_lock SET value = 22 WHERE id = 2;
```

SESSION_3:
```
BEGIN;
UPDATE dead_lock SET value = 33 WHERE id = 3;
```

- Повесили по локу на каждую таблицу - 3 сессии - 3 лока - 3 строки

- Повторно проходим по сессиям:

SESSION_1:
```
UPDATE dead_lock SET value = 2 WHERE id = 2;
```

SESSION_2:
```
UPDATE dead_lock SET value = 3 WHERE id = 3;
```

SESSION_3:
```
UPDATE dead_lock SET value = 1 WHERE id = 1;
```

- После выполнения команды в 3-ей сессии получили дедлок, о чем посгрес и сказал:
```
Process 6643 waits for ShareLock on transaction 819; blocked by process 6658.
Process 6658 waits for ShareLock on transaction 820; blocked by process 6672.
Process 6672: update test set value = 1 where id =1;
Process 6643: update test set value = 2 where id =2;
Process 6658: update test set value = 3 where id =3;
```

Запись эту увидели как в терминале 3-ей сессии, так и в логах посгреса.

- В итоге:

3-ья сессия упала с ошибкой

1-ая заблокирована

2-ая разблокировалась

- Делаем коммит во 2-ой сессии, тем самым разблокируем 1-ую сессию.

#### 4. Могут ли две транзакции, выполняющие единственную команду UPDATE одной и той же таблицы (без where), заблокировать друг друга? Попробуйте воспроизвести такую ситуацию.

- Начнем 2 сессии:

SESSION_1
```
test_db=# BEGIN;
BEGIN
test_db=*# SELECT txid_current(), pg_backend_pid();
 txid_current | pg_backend_pid 
--------------+----------------
          859 |           45
```

SESSION_2
```
test_db=# BEGIN;
BEGIN
test_db=*# SELECT txid_current(), pg_backend_pid();
 txid_current | pg_backend_pid 
--------------+----------------
          860 |           59
(1 row)
```

- Попробуем выполнить UPDATE всех строк без указания условий

SESSION_1
```
UPDATE test_lock SET value = value + 100;
UPDATE 3
```

Тут мы не заблокировались.

SESSION_2
```
UPDATE test_lock SET value = value + 100;
```

Поймали блокировку.
Посмотрим, что находится в ```pg_locks```

SESSION_1 (для первой транзакции)
```
SELECT locktype, relation::REGCLASS, virtualxid, transactionid, mode, granted  FROM pg_locks WHERE pid = 45;
   locktype    | relation  | virtualxid | transactionid |       mode       | granted 
---------------+-----------+------------+---------------+------------------+---------
 relation      | pg_locks  |            |               | AccessShareLock  | t
 relation      | test_lock |            |               | RowExclusiveLock | t
 virtualxid    |           | 3/2        |               | ExclusiveLock    | t
 transactionid |           |            |           859 | ExclusiveLock    | t
```

В данной транзакции у нас мы захватили все блокировки.

SESSION_1 (для второй транзакции)
```
SELECT locktype, relation::REGCLASS, virtualxid, transactionid, mode, granted  FROM pg_locks WHERE pid = 59;
   locktype    | relation  | virtualxid | transactionid |       mode       | granted 
---------------+-----------+------------+---------------+------------------+---------
 relation      | test_lock |            |               | RowExclusiveLock | t
 virtualxid    |           | 4/2        |               | ExclusiveLock    | t
 transactionid |           |            |           860 | ExclusiveLock    | t
 transactionid |           |            |           859 | ShareLock        | f
 tuple         | test_lock |            |               | ExclusiveLock    | t
```

А тут ждем освобождения блокировки режима ```ShareLock```

- Попробовал перед UPDATE добавить выборку

- Начинаем новые сессии
  SESSION_1
```
test_db=# BEGIN;
BEGIN
test_db=*# SELECT txid_current(), pg_backend_pid();
 txid_current | pg_backend_pid 
--------------+----------------
          859 |           45
```

SESSION_2
```
test_db=# BEGIN;
BEGIN
test_db=*# SELECT txid_current(), pg_backend_pid();
 txid_current | pg_backend_pid 
--------------+----------------
          860 |           59
(1 row)
```

SESSION_1
```
test_db=*# SELECT * FROM test_lock FOR UPDATE;
 id | value 
----+-------
  2 |   222
  3 |   333
  1 |     1
(3 rows)
```

SESSION_2
```
UPDATE test_lock SET value = value + 100;
```
Тут заблокировались.

SESSION_1
```
test_db=*# UPDATE test_lock SET value = 123;
UPDATE 3
```

Тут снова без блокировки получилось обновить данные.


Кажется, что не получится добиться именно ВЗАИМНОЙ блокировки в рамках одной таблицы без указания предиката.
Потому что первая транзакция, которая захватила доступ к таблице получаем экслюзивную блокировку на нее,
которую другие транзакции уже не могут перехватить и только отправляются в режим ожидания разблокировки.

