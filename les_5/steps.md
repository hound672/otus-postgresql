#### 1. Создал ВМ, установил Postgres 14

#### 2. Изменил настройки согласено тем, которые были в материал к лекции

#### 3. выполнить pgbench -i postgres

Done

#### 4. запустить pgbench -c8 -P 60 -T 3600 -U postgres postgres

Done

#### 5. дать отработать до конца

waiting...

#### 6. зафиксировать среднее значение tps в последней ⅙ части работы

Полный вывод:
```
progress: 60.0 s, 545.1 tps, lat 14.623 ms stddev 13.743
progress: 120.0 s, 299.2 tps, lat 26.699 ms stddev 13.535
progress: 180.0 s, 299.2 tps, lat 26.693 ms stddev 13.889
progress: 240.0 s, 298.0 tps, lat 26.808 ms stddev 13.586
progress: 300.0 s, 296.1 tps, lat 26.973 ms stddev 14.126
progress: 360.0 s, 297.0 tps, lat 26.889 ms stddev 14.137
progress: 420.0 s, 296.0 tps, lat 26.980 ms stddev 15.514
progress: 480.0 s, 297.4 tps, lat 26.853 ms stddev 13.864
progress: 540.0 s, 297.1 tps, lat 26.881 ms stddev 14.138
progress: 600.0 s, 296.8 tps, lat 26.904 ms stddev 13.814
progress: 660.0 s, 297.8 tps, lat 26.816 ms stddev 13.843
progress: 720.0 s, 297.9 tps, lat 26.810 ms stddev 13.806
progress: 780.0 s, 297.4 tps, lat 26.851 ms stddev 13.805
progress: 840.0 s, 296.8 tps, lat 26.909 ms stddev 14.238
progress: 900.0 s, 296.7 tps, lat 26.915 ms stddev 13.990
progress: 960.0 s, 296.0 tps, lat 26.979 ms stddev 15.493
progress: 1020.0 s, 297.9 tps, lat 26.809 ms stddev 13.886
progress: 1080.0 s, 297.0 tps, lat 26.889 ms stddev 14.083
progress: 1140.0 s, 297.9 tps, lat 26.805 ms stddev 13.821
progress: 1200.0 s, 296.8 tps, lat 26.902 ms stddev 13.938
progress: 1260.0 s, 297.8 tps, lat 26.822 ms stddev 13.877
progress: 1320.0 s, 297.9 tps, lat 26.808 ms stddev 13.628
progress: 1380.0 s, 296.6 tps, lat 26.927 ms stddev 14.030
progress: 1440.0 s, 297.2 tps, lat 26.875 ms stddev 14.076
progress: 1500.0 s, 296.7 tps, lat 26.919 ms stddev 13.841
progress: 1560.0 s, 297.8 tps, lat 26.813 ms stddev 13.984
progress: 1620.0 s, 297.9 tps, lat 26.808 ms stddev 13.647
progress: 1680.0 s, 296.5 tps, lat 26.932 ms stddev 14.110
progress: 1740.0 s, 297.8 tps, lat 26.813 ms stddev 13.859
progress: 1800.0 s, 296.9 tps, lat 26.895 ms stddev 14.086
progress: 1860.0 s, 297.7 tps, lat 26.825 ms stddev 14.016
progress: 1920.0 s, 297.3 tps, lat 26.868 ms stddev 14.071
progress: 1980.0 s, 296.7 tps, lat 26.916 ms stddev 14.117
progress: 2040.0 s, 297.3 tps, lat 26.859 ms stddev 14.359
progress: 2100.0 s, 296.9 tps, lat 26.903 ms stddev 13.873
progress: 2160.0 s, 297.7 tps, lat 26.823 ms stddev 13.988
progress: 2220.0 s, 297.9 tps, lat 26.802 ms stddev 13.747
progress: 2280.0 s, 296.7 tps, lat 26.915 ms stddev 14.343
progress: 2340.0 s, 297.9 tps, lat 26.812 ms stddev 13.776
progress: 2400.0 s, 296.9 tps, lat 26.891 ms stddev 13.778
progress: 2460.0 s, 297.8 tps, lat 26.820 ms stddev 13.765
progress: 2520.0 s, 297.8 tps, lat 26.813 ms stddev 13.950
progress: 2580.0 s, 296.9 tps, lat 26.894 ms stddev 14.166
progress: 2640.0 s, 297.3 tps, lat 26.859 ms stddev 14.187
progress: 2700.0 s, 296.8 tps, lat 26.903 ms stddev 14.099
progress: 2760.0 s, 297.8 tps, lat 26.815 ms stddev 13.812
progress: 2820.0 s, 297.6 tps, lat 26.829 ms stddev 13.907
progress: 2880.0 s, 297.1 tps, lat 26.884 ms stddev 14.060
progress: 2940.0 s, 298.2 tps, lat 26.780 ms stddev 13.758
progress: 3000.0 s, 296.9 tps, lat 26.894 ms stddev 14.128
progress: 3060.0 s, 297.8 tps, lat 26.814 ms stddev 13.811
progress: 3120.0 s, 297.5 tps, lat 26.844 ms stddev 13.883
progress: 3180.0 s, 296.7 tps, lat 26.916 ms stddev 14.023
progress: 3240.0 s, 296.8 tps, lat 26.901 ms stddev 14.233
progress: 3300.0 s, 297.1 tps, lat 26.880 ms stddev 13.971
progress: 3360.0 s, 297.8 tps, lat 26.811 ms stddev 14.301
progress: 3420.0 s, 297.8 tps, lat 26.816 ms stddev 13.851
progress: 3480.0 s, 297.3 tps, lat 26.865 ms stddev 13.943
progress: 3540.0 s, 298.3 tps, lat 26.773 ms stddev 13.593
progress: 3600.0 s, 296.8 tps, lat 26.903 ms stddev 14.029
transaction type: <builtin: TPC-B (sort of)>
scaling factor: 1
query mode: simple
number of clients: 8
number of threads: 1
duration: 3600 s
number of transactions actually processed: 1085291
latency average = 26.489 ms
latency stddev = 14.154 ms
initial connection time = 23.126 ms
tps = 301.468895 (without initial connection time)
```

Разница между первым и вторым значением tps почти в 2 раза.
Возможно, что это связано с тем, что на момент начала теста в БД еще не было "мертвых" записей?

Среднее значение tps по последней 1/6 части теста: 298,11
Деградация по срвнению с началом теста (2-ой записью) не сильно большая.


#### 7. а дальше настроить autovacuum максимально эффективно

1. Значение параметра ```autovacuum_vacuum_scale_factor``` было равно 0.2
Попробую поставить значение 0.1, кажется, что так будет быстрее работать автовакум. Для начала просто проверю какой
эффект от этого будет. (0.1 поставил из тех соображение, что во время бенча проихводится много update'ов
и из-за этого могут много плодится "мертвых" записей, которые могут тормозить вставку - как мне кажется из-за 
пересчета индексов при вставке - верно ли это?)


Новые полученные значения:
```
progress: 60.0 s, 543.8 tps, lat 14.656 ms stddev 12.108
progress: 120.0 s, 298.6 tps, lat 26.739 ms stddev 14.027
progress: 180.0 s, 299.4 tps, lat 26.674 ms stddev 13.640
progress: 240.0 s, 298.6 tps, lat 26.742 ms stddev 14.098
progress: 300.0 s, 297.4 tps, lat 26.849 ms stddev 13.926
progress: 360.0 s, 297.4 tps, lat 26.850 ms stddev 13.993
progress: 420.0 s, 297.8 tps, lat 26.815 ms stddev 13.908
progress: 480.0 s, 297.4 tps, lat 26.848 ms stddev 14.098
progress: 540.0 s, 296.5 tps, lat 26.938 ms stddev 14.379
progress: 600.0 s, 297.6 tps, lat 26.828 ms stddev 14.115
progress: 660.0 s, 297.5 tps, lat 26.841 ms stddev 14.080
progress: 720.0 s, 297.2 tps, lat 26.868 ms stddev 14.261
progress: 780.0 s, 297.3 tps, lat 26.854 ms stddev 13.931
progress: 840.0 s, 297.8 tps, lat 26.821 ms stddev 13.924
progress: 900.0 s, 297.5 tps, lat 26.841 ms stddev 14.015
progress: 960.0 s, 297.8 tps, lat 26.818 ms stddev 13.862
progress: 1020.0 s, 297.8 tps, lat 26.813 ms stddev 13.833
progress: 1080.0 s, 297.5 tps, lat 26.844 ms stddev 13.987
progress: 1140.0 s, 297.7 tps, lat 26.820 ms stddev 13.653
progress: 1200.0 s, 297.6 tps, lat 26.838 ms stddev 13.941
progress: 1260.0 s, 297.5 tps, lat 26.841 ms stddev 14.038
progress: 1320.0 s, 297.1 tps, lat 26.875 ms stddev 14.267
progress: 1380.0 s, 297.5 tps, lat 26.841 ms stddev 13.903
progress: 1440.0 s, 297.7 tps, lat 26.824 ms stddev 13.914
progress: 1500.0 s, 297.7 tps, lat 26.817 ms stddev 14.073
progress: 1560.0 s, 297.6 tps, lat 26.835 ms stddev 13.948
progress: 1620.0 s, 297.7 tps, lat 26.824 ms stddev 13.870
progress: 1680.0 s, 297.5 tps, lat 26.839 ms stddev 14.158
progress: 1740.0 s, 297.7 tps, lat 26.820 ms stddev 14.046
progress: 1800.0 s, 297.5 tps, lat 26.845 ms stddev 13.907
progress: 1860.0 s, 297.7 tps, lat 26.828 ms stddev 13.729
progress: 1920.0 s, 297.2 tps, lat 26.873 ms stddev 14.146
progress: 1980.0 s, 297.4 tps, lat 26.847 ms stddev 14.013
progress: 2040.0 s, 297.8 tps, lat 26.810 ms stddev 14.146
progress: 2100.0 s, 297.6 tps, lat 26.831 ms stddev 13.913
progress: 2160.0 s, 297.6 tps, lat 26.838 ms stddev 13.968
progress: 2220.0 s, 297.8 tps, lat 26.815 ms stddev 13.824
progress: 2280.0 s, 297.4 tps, lat 26.848 ms stddev 14.159
progress: 2340.0 s, 297.0 tps, lat 26.893 ms stddev 14.264
progress: 2400.0 s, 297.4 tps, lat 26.850 ms stddev 14.124
progress: 2460.0 s, 297.8 tps, lat 26.817 ms stddev 14.070
progress: 2520.0 s, 297.0 tps, lat 26.890 ms stddev 14.388
progress: 2580.0 s, 297.3 tps, lat 26.853 ms stddev 14.082
progress: 2640.0 s, 297.8 tps, lat 26.819 ms stddev 13.879
progress: 2700.0 s, 297.0 tps, lat 26.890 ms stddev 14.023
progress: 2760.0 s, 297.5 tps, lat 26.845 ms stddev 13.668
progress: 2820.0 s, 297.1 tps, lat 26.881 ms stddev 14.034
progress: 2880.0 s, 297.3 tps, lat 26.856 ms stddev 14.158
progress: 2940.0 s, 297.7 tps, lat 26.822 ms stddev 13.868
progress: 3000.0 s, 296.8 tps, lat 26.901 ms stddev 13.938
progress: 3060.0 s, 297.8 tps, lat 26.822 ms stddev 13.876
progress: 3120.0 s, 297.2 tps, lat 26.871 ms stddev 14.439
progress: 3180.0 s, 297.5 tps, lat 26.849 ms stddev 14.109
progress: 3240.0 s, 297.7 tps, lat 26.825 ms stddev 13.976
progress: 3300.0 s, 297.9 tps, lat 26.811 ms stddev 13.764
progress: 3360.0 s, 297.6 tps, lat 26.830 ms stddev 13.965
progress: 3420.0 s, 297.8 tps, lat 26.816 ms stddev 13.687
progress: 3480.0 s, 297.4 tps, lat 26.854 ms stddev 13.982
progress: 3540.0 s, 296.6 tps, lat 26.922 ms stddev 14.572
progress: 3600.0 s, 297.3 tps, lat 26.861 ms stddev 13.926
transaction type: <builtin: TPC-B (sort of)>
scaling factor: 1
query mode: simple
number of clients: 8
number of threads: 1
duration: 3600 s
number of transactions actually processed: 1085942
latency average = 26.472 ms
latency stddev = 14.110 ms
initial connection time = 23.030 ms
tps = 301.650441 (without initial connection time)
```

tps стал ровнее, и среднее значение (которое отображается в самом конце вывода).

Проверим кол-во и процентное соотношение "мертвых" туплов после бенча в таблицах, которые используются в нагрузочном тесте.
```
SELECT relname, n_live_tup, n_dead_tup, trunc(100*n_dead_tup/(n_live_tup+1))::float "ratio%", last_autovacuum FROM pg_stat_user_TABLEs WHERE relname = 'pgbench_accounts';
9577 - 9%
```

```
SELECT relname, n_live_tup, n_dead_tup, trunc(100*n_dead_tup/(n_live_tup+1))::float "ratio%", last_autovacuum FROM pg_stat_user_TABLEs WHERE relname = 'pgbench_tellers';
0 - 0%
```

```
SELECT relname, n_live_tup, n_dead_tup, trunc(100*n_dead_tup/(n_live_tup+1))::float "ratio%", last_autovacuum FROM pg_stat_user_TABLEs WHERE relname = 'pgbench_history';
0 - 0%
```

Видим, что у первой таблице довольно большое кол-во мертвых записей.
9%, а в конфигурации autovaccum я поставил 10% - верно?

Но значение ```autovacuum_vacuum_threshold``` равно 50, но почему тогда кол-во мертвых туплов больше 50%
Разве этот параметр не определяет кол-во мертвых туплов после которого запускается вакуум?

Поменяем значение параметра ```autovacuum_vacuum_threshold``` на 25.

И запустим снова pgbench (использую nohup т.к тест выполнется довольно долго и чтобы исключить аварийное завершение ssh
сессии из-за сетевых проблем):
```
nohup pgbench -c8 -P 60 -T 3600 -U postgres postgres &
```

Получили следующие данные:
```
progress: 60.0 s, 533.4 tps, lat 14.941 ms stddev 11.180
progress: 120.0 s, 299.3 tps, lat 26.674 ms stddev 13.974
progress: 180.0 s, 281.1 tps, lat 28.404 ms stddev 22.064
progress: 240.0 s, 297.6 tps, lat 26.824 ms stddev 13.964
progress: 300.0 s, 297.8 tps, lat 26.808 ms stddev 13.860
progress: 360.0 s, 296.6 tps, lat 26.921 ms stddev 14.013
progress: 420.0 s, 298.8 tps, lat 26.713 ms stddev 13.903
progress: 480.0 s, 296.5 tps, lat 26.924 ms stddev 13.988
progress: 540.0 s, 297.7 tps, lat 26.814 ms stddev 13.931
progress: 600.0 s, 297.0 tps, lat 26.889 ms stddev 14.346
progress: 660.0 s, 297.4 tps, lat 26.850 ms stddev 14.101
progress: 720.0 s, 298.2 tps, lat 26.777 ms stddev 14.149
progress: 780.0 s, 296.5 tps, lat 26.922 ms stddev 13.958
progress: 840.0 s, 297.9 tps, lat 26.800 ms stddev 13.853
progress: 900.0 s, 297.7 tps, lat 26.820 ms stddev 13.815
progress: 960.0 s, 297.2 tps, lat 26.873 ms stddev 13.839
progress: 1020.0 s, 298.3 tps, lat 26.766 ms stddev 13.896
progress: 1080.0 s, 296.6 tps, lat 26.918 ms stddev 14.115
progress: 1140.0 s, 297.3 tps, lat 26.860 ms stddev 14.002
progress: 1200.0 s, 296.8 tps, lat 26.894 ms stddev 14.488
progress: 1260.0 s, 296.7 tps, lat 26.911 ms stddev 14.266
progress: 1320.0 s, 294.1 tps, lat 27.147 ms stddev 14.928
progress: 1380.0 s, 296.2 tps, lat 26.957 ms stddev 14.231
progress: 1440.0 s, 297.1 tps, lat 26.873 ms stddev 14.272
progress: 1500.0 s, 297.0 tps, lat 26.880 ms stddev 14.347
progress: 1560.0 s, 296.7 tps, lat 26.908 ms stddev 14.448
progress: 1620.0 s, 297.7 tps, lat 26.818 ms stddev 14.061
progress: 1680.0 s, 295.9 tps, lat 26.981 ms stddev 14.439
progress: 1740.0 s, 297.1 tps, lat 26.873 ms stddev 14.146
progress: 1800.0 s, 296.8 tps, lat 26.903 ms stddev 14.345
progress: 1860.0 s, 296.7 tps, lat 26.912 ms stddev 14.151
progress: 1920.0 s, 297.7 tps, lat 26.824 ms stddev 14.114
progress: 1980.0 s, 296.1 tps, lat 26.965 ms stddev 14.580
progress: 2040.0 s, 296.9 tps, lat 26.893 ms stddev 14.199
progress: 2100.0 s, 297.1 tps, lat 26.870 ms stddev 14.118
progress: 2160.0 s, 297.3 tps, lat 26.858 ms stddev 14.025
progress: 2220.0 s, 297.5 tps, lat 26.835 ms stddev 14.299
progress: 2280.0 s, 296.2 tps, lat 26.953 ms stddev 14.474
progress: 2340.0 s, 297.0 tps, lat 26.878 ms stddev 14.227
progress: 2400.0 s, 296.7 tps, lat 26.913 ms stddev 14.319
progress: 2460.0 s, 296.9 tps, lat 26.887 ms stddev 14.043
progress: 2520.0 s, 297.6 tps, lat 26.830 ms stddev 14.144
progress: 2580.0 s, 295.8 tps, lat 26.991 ms stddev 14.533
progress: 2640.0 s, 297.1 tps, lat 26.874 ms stddev 14.232
progress: 2700.0 s, 297.3 tps, lat 26.850 ms stddev 14.169
progress: 2760.0 s, 296.7 tps, lat 26.910 ms stddev 14.224
progress: 2820.0 s, 298.2 tps, lat 26.769 ms stddev 14.141
progress: 2880.0 s, 296.0 tps, lat 26.970 ms stddev 14.476
progress: 2940.0 s, 297.1 tps, lat 26.877 ms stddev 14.375
progress: 3000.0 s, 296.2 tps, lat 26.959 ms stddev 14.703
progress: 3060.0 s, 297.1 tps, lat 26.874 ms stddev 14.099
progress: 3120.0 s, 296.8 tps, lat 26.902 ms stddev 14.513
progress: 3180.0 s, 295.9 tps, lat 26.983 ms stddev 14.442
progress: 3240.0 s, 297.3 tps, lat 26.859 ms stddev 14.136
progress: 3300.0 s, 297.0 tps, lat 26.881 ms stddev 14.156
progress: 3360.0 s, 296.6 tps, lat 26.920 ms stddev 14.429
progress: 3420.0 s, 298.3 tps, lat 26.768 ms stddev 14.085
progress: 3480.0 s, 296.0 tps, lat 26.980 ms stddev 14.376
progress: 3540.0 s, 297.0 tps, lat 26.885 ms stddev 14.132
progress: 3600.0 s, 296.6 tps, lat 26.921 ms stddev 14.492
transaction type: <builtin: TPC-B (sort of)>
scaling factor: 1
query mode: simple
number of clients: 8
number of threads: 1
duration: 3600 s
number of transactions actually processed: 1082519
latency average = 26.551 ms
latency stddev = 14.430 ms
initial connection time = 24.351 ms
tps = 300.699261 (without initial connection time)
```

Стало хуже - откатываемк конфигурацию.
