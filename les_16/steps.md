### PostgreSQL и Google Kubernetes Engine

#### 1. Подготовка

Создаем кластер GKE

```
gcloud beta container --project "postgres2021-19890604" clusters create "cluster-1" --zone "us-central1-c" --no-enable-basic-auth --cluster-version "1.21.5-gke.1302" --release-channel "regular" --machine-type "e2-medium" --image-type "COS_CONTAINERD" --disk-type "pd-standard" --disk-size "100" --metadata disable-legacy-endpoints=true --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --max-pods-per-node "110" --num-nodes "3" --logging=SYSTEM,WORKLOAD --monitoring=SYSTEM --enable-ip-alias --network "projects/postgres2021-19890604/global/networks/default" --subnetwork "projects/postgres2021-19890604/regions/us-central1/subnetworks/default" --no-enable-intra-node-visibility --default-max-pods-per-node "110" --no-enable-master-authorized-networks --addons HorizontalPodAutoscaling,HttpLoadBalancing,GcePersistentDiskCsiDriver --enable-autoupgrade --enable-autorepair --max-surge-upgrade 1 --max-unavailable-upgrade 0 --enable-shielded-nodes --node-locations "us-central1-c"
```

P.S. Я не люблю ставить подобные зависимости на свою машину (хоть и линукса основная система), поэтому разворачиваю виртуалку и там ставлю.
Или вообще использую докер, пустой образ на базе Убунты, в котором уже ставлю всякие подобные утилиты.

P.P.S. Я заметил, что Вы на лекциях используете терминал и переключение по вкладкам. 
Я как-то открыл для себя ```tmux``` у него кроме поддержок вкладок есть поддержка сплита одной вкладки.
Можно один экран "дробить" на N-ое кол-во терминалов.

Даем права тулзе ```kubectl```:

```
gcloud container clusters get-credentials postgres2021-19890604
```

Посмотрим список созданных контейнеров:

```
virtual@ubuntu20:~$ gcloud container clusters list
NAME       LOCATION       MASTER_VERSION   MASTER_IP       MACHINE_TYPE  NODE_VERSION     NUM_NODES  STATUS
cluster-1  us-central1-c  1.21.5-gke.1302  35.193.255.202  e2-medium     1.21.5-gke.1302  3          RUNNING
```

Еще смотрим инфу:

```
virtual@ubuntu20:~$ kubectl cluster-info
Kubernetes control plane is running at https://35.193.255.202
GLBCDefaultBackend is running at https://35.193.255.202/api/v1/namespaces/kube-system/services/default-http-backend:http/proxy
KubeDNS is running at https://35.193.255.202/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
Metrics-server is running at https://35.193.255.202/api/v1/namespaces/kube-system/services/https:metrics-server:/proxy

virtual@ubuntu20:~/psql/citus-k8s$ kubectl get nodes
NAME                                       STATUS   ROLES    AGE     VERSION
gke-cluster-1-default-pool-66670cf3-1mkk   Ready    <none>   4m51s   v1.21.5-gke.1302
gke-cluster-1-default-pool-66670cf3-2gzh   Ready    <none>   4m51s   v1.21.5-gke.1302
gke-cluster-1-default-pool-66670cf3-mvrw   Ready    <none>   4m51s   v1.21.5-gke.1302
```

#### 2. Работа с citus

Клонируем репу: 
```
git clone https://github.com/aeuge/citus-k8s
```

Прописываем закодированный пароль в ```secrets.yaml```.

Создаем "секреты"

```
virtual@ubuntu20:~/psql/citus-k8s$ kubectl create -f secrets.yaml
secret/citus-secrets created
```

Создаем мастера:

```
virtual@ubuntu20:~/psql/citus-k8s$ kubectl create -f master.yaml
persistentvolumeclaim/citus-master-pvc created
service/citus-master created
deployment.apps/citus-master created
```

Посмотрим, что там сейчас создалось:

```
virtual@ubuntu20:~/psql/citus-k8s$ kubectl get all
NAME                                READY   STATUS    RESTARTS   AGE
pod/citus-master-78ff549b8f-xdj6h   1/1     Running   0          79s

NAME                   TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
service/citus-master   ClusterIP   None         <none>        5432/TCP   80s
service/kubernetes     ClusterIP   10.8.0.1     <none>        443/TCP    16m

NAME                           READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/citus-master   1/1     1            1           80s

NAME                                      DESIRED   CURRENT   READY   AGE
replicaset.apps/citus-master-78ff549b8f   1         1         1       80s
```

Мастер у нас запустился, приступаем к запуску воркеров:

```
virtual@ubuntu20:~/psql/citus-k8s$ kubectl create -f workers.yaml
statefulset.apps/citus-worker created
```

Смотрим статус еще раз:

```
virtual@ubuntu20:~/psql/citus-k8s$ kubectl get all
NAME                                READY   STATUS              RESTARTS   AGE
pod/citus-master-78ff549b8f-xdj6h   1/1     Running             0          5m18s
pod/citus-worker-0                  1/1     Running             0          51s
pod/citus-worker-1                  1/1     Running             0          39s
pod/citus-worker-2                  0/1     ContainerCreating   0          11s

NAME                    TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
service/citus-master    ClusterIP   None         <none>        5432/TCP   5m20s
service/citus-workers   ClusterIP   None         <none>        5432/TCP   2m20s
service/kubernetes      ClusterIP   10.8.0.1     <none>        443/TCP    20m

NAME                           READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/citus-master   1/1     1            1           5m19s

NAME                                      DESIRED   CURRENT   READY   AGE
replicaset.apps/citus-master-78ff549b8f   1         1         1       5m19s

NAME                            READY   AGE
statefulset.apps/citus-worker   2/3     52s
```

Через пол минуты проверяем еще раз:
```
virtual@ubuntu20:~/psql/citus-k8s$ kubectl get all
NAME                                READY   STATUS    RESTARTS   AGE
pod/citus-master-78ff549b8f-xdj6h   1/1     Running   0          5m52s
pod/citus-worker-0                  1/1     Running   0          85s
pod/citus-worker-1                  1/1     Running   0          73s
pod/citus-worker-2                  1/1     Running   0          45s

NAME                    TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
service/citus-master    ClusterIP   None         <none>        5432/TCP   5m53s
service/citus-workers   ClusterIP   None         <none>        5432/TCP   2m53s
service/kubernetes      ClusterIP   10.8.0.1     <none>        443/TCP    21m

NAME                           READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/citus-master   1/1     1            1           5m52s

NAME                                      DESIRED   CURRENT   READY   AGE
replicaset.apps/citus-master-78ff549b8f   1         1         1       5m52s

NAME                            READY   AGE
statefulset.apps/citus-worker   3/3     85s
```

Теперь все запустилось.

Пробуем зайти в мастер, для этого нужно запомнить имя мастера: ```pod/citus-master-78ff549b8f-xdj6h```

```
virtual@ubuntu20:~/psql/citus-k8s$ kubectl exec -it pod/citus-master-78ff549b8f-xdj6h -- psql -U postgres
psql (10.3 (Debian 10.3-1.pgdg90+1))
Type "help" for help.

postgres=#
```

Мы успешно зашли.

Смоотрим статус нод:
```
postgres=# SELECT * FROM master_get_active_worker_nodes();
          node_name           | node_port
------------------------------+-----------
 citus-worker-2.citus-workers |      5432
 citus-worker-0.citus-workers |      5432
 citus-worker-1.citus-workers |      5432
(3 rows)
```

Создаем базу данных:

```
postgres=# create database test;
NOTICE:  Citus partially supports CREATE DATABASE for distributed databases
DETAIL:  Citus does not propagate CREATE DATABASE command to workers
HINT:  You can manually create a database and its extensions on workers.
CREATE DATABASE
```

После чего создаем в каждом воркере эту БД, включаем расширение и создаем таблицу:
```
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE test (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v1(),
    Region VARCHAR(50),
    Country VARCHAR(50),
    ItemType VARCHAR(50),
    SalesChannel VARCHAR(20),
    OrderPriority VARCHAR(10),
    OrderDate VARCHAR(10),
    OrderID int,
    ShipDate VARCHAR(10),
    UnitsSold int,
    UnitPrice decimal(12,2),
    UnitCost decimal(12,2),
    TotalRevenue decimal(12,2),
    TotalCost decimal(12,2),
    TotalProfit decimal(12,2)
);
```

И подключчем на каждом воркере расширение:
```
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
```

На мастер ноду скачиваем датасет:

```
wget https://storage.googleapis.com/postgres13/1000000SalesRecords.csv
```

Я его чуть дополнил, чтобы было больше данных.

И загружаем его с помощью ```copy``` на мастер ноде.

```
SELECT create_distributed_table('test', 'id');
SELECT rebalance_table_shards('test');
```

Смотрим что у нас на воркерах:

```
postgres=# \d
            List of relations
 Schema |    Name     | Type  |  Owner
--------+-------------+-------+----------
 public | test        | table | postgres
 public | test_102136 | table | postgres
 public | test_102139 | table | postgres
 public | test_102142 | table | postgres
 public | test_102145 | table | postgres
 public | test_102148 | table | postgres
 public | test_102151 | table | postgres
 public | test_102154 | table | postgres
 public | test_102157 | table | postgres
 public | test_102160 | table | postgres
 public | test_102163 | table | postgres
 public | test_102166 | table | postgres
(12 rows)

postgres=# select count(*) from test_102163;
 count
-------
 31547
(1 row)
```

И похожая картина на всех воркерах.

Значит мы успешно шардировали нашу базу по воркерам.

Удаляем все...

```
kubectl delete all --all 
```
