### Развернуть CockroachDB в GCE

#### 1. Создаем 3 ВМ

#### 2. Ставим cockroach:

На всех трех инстансах:
```
wget -qO- https://binaries.cockroachdb.com/cockroach-v21.1.6.linux-amd64.tgz | tar  xvz && sudo cp -i cockroach-v21.1.6.linux-amd64/cockroach /usr/local/bin/ && sudo mkdir -p /opt/cockroach && sudo chown hound672:hound672 /opt/cockroach
```

#### 3. Генерируем сертификат

```
mkdir certs my-safe-directory
cockroach cert create-ca --certs-dir=certs --ca-key=my-safe-directory/ca.key
cockroach cert create-node localhost cockroach-1 cockroach-2 cockroach-3 --certs-dir=certs --ca-key=my-safe-directory/ca.key --overwrite
cockroach cert create-client root --certs-dir=certs --ca-key=my-safe-directory/ca.key
```

Копируем серты себе на локальную машину

На локальной машине:
```
mkdir -p /tmp/certs
cd /tmp/certs

$ scp -r gce-cockroach-2:/home/hound672/certs .                                                                                                              
client.root.crt                                                                                                                                   100% 1143   221.6KB/s   00:00
ca.crt                                                                                                                                            100% 1151   146.3KB/s   00:00
node.crt                                                                                                                                          100% 1229   247.1KB/s   00:00
client.root.key                                                                                                                                   100% 1675   332.1KB/s   00:00
node.key            

$ ls certs                                                                                                                                                    ✔
ca.crt  client.root.crt  client.root.key  node.crt  node.key
```

Теперь копируем их на оставшиеся 2 инстанса

```
scp -r certs gce-cockroach-1:~
scp -r certs gce-cockroach-3:~
```

Выставлем права:
```
chmod 700 ~/certs/*
```

#### 4. Запуск

На первой ноде запускаем:

```
hound672@cockroach-1:~$ cockroach start --certs-dir=certs --advertise-addr=cockroach-1 --join=cockroach-1,cockroach-2,cockroach-3 --cache=.25 --max-sql-memory=.25 --background
*
* INFO: initial startup completed.
* Node will now attempt to join a running cluster, or wait for `cockroach init`.
* Client connections will be accepted after this completes successfully.
* Check the log file(s) for progress.
*

```

На второй:
```
hound672@cockroach-2:~$ cockroach start --certs-dir=certs --advertise-addr=cockroach-2 --join=cockroach-1,cockroach-2,cockroach-3 --cache=.25 --max-sql-memory=.25 --background
*
* INFO: initial startup completed.
* Node will now attempt to join a running cluster, or wait for `cockroach init`.
* Client connections will be accepted after this completes successfully.
* Check the log file(s) for progress.
*
```

На третьей:
```
hound672@cockroach-3:~$ cockroach start --certs-dir=certs --advertise-addr=cockroach-3 --join=cockroach-1,cockroach-2,cockroach-3 --cache=.25 --max-sql-memory=.25 --background
*
* INFO: initial startup completed.
* Node will now attempt to join a running cluster, or wait for `cockroach init`.
* Client connections will be accepted after this completes successfully.
* Check the log file(s) for progress.
*
```

Инициализируем кластер

```
hound672@cockroach-1:~$ cockroach init --certs-dir=certs --host=cockroach-1
Cluster successfully initialized
```

Смотрим статус:
```
hound672@cockroach-1:~$ cockroach node status --certs-dir=certs
  id |      address      |    sql_address    |  build  |         started_at         |         updated_at         | locality | is_available | is_live
-----+-------------------+-------------------+---------+----------------------------+----------------------------+----------+--------------+----------
   1 | cockroach-1:26257 | cockroach-1:26257 | v21.1.6 | 2022-01-20 16:21:35.715163 | 2022-01-20 16:22:38.913369 |          | true         | true
   2 | cockroach-2:26257 | cockroach-2:26257 | v21.1.6 | 2022-01-20 16:21:36.534445 | 2022-01-20 16:22:39.678009 |          | true         | true
   3 | cockroach-3:26257 | cockroach-3:26257 | v21.1.6 | 2022-01-20 16:21:36.535123 | 2022-01-20 16:22:39.671235 |          | true         | true
(3 rows)
```

#### 5. Работа с данными

Подключаемся через клиент и создаем базу:

```
hound672@cockroach-1:~$ cockroach sql --certs-dir=certs
#
# Welcome to the CockroachDB SQL shell.
# All statements must be terminated by a semicolon.
# To exit, type: \q.
#
# Server version: CockroachDB CCL v21.1.6 (x86_64-unknown-linux-gnu, built 2021/07/20 15:30:39, go1.15.11) (same version as client)
# Cluster ID: 5fcdaba7-1ae3-4da6-a679-587faa72e6a9
No entry for terminal type "screen-256color";
using dumb terminal settings.
#
# Enter \? for a brief introduction.
#
root@:26257/defaultdb> CREATE DATABASE test_db;
CREATE DATABASE

Time: 20ms total (execution 20ms / network 0ms)
```

Проверяем на другом инстансе
```
hound672@cockroach-2:~$ cockroach sql --certs-dir=certs
#
# Welcome to the CockroachDB SQL shell.
# All statements must be terminated by a semicolon.
# To exit, type: \q.
#
# Server version: CockroachDB CCL v21.1.6 (x86_64-unknown-linux-gnu, built 2021/07/20 15:30:39, go1.15.11) (same version as client)
# Cluster ID: 5fcdaba7-1ae3-4da6-a679-587faa72e6a9
No entry for terminal type "screen-256color";
using dumb terminal settings.
#
# Enter \? for a brief introduction.
#
root@:26257/defaultdb> \l
  database_name | owner | primary_region | regions | survival_goal
----------------+-------+----------------+---------+----------------
  defaultdb     | root  | NULL           | {}      | NULL
  postgres      | root  | NULL           | {}      | NULL
  system        | node  | NULL           | {}      | NULL
  test_db       | root  | NULL           | {}      | NULL
(4 rows)

Time: 4ms total (execution 4ms / network 0ms)
```


#### 6. Загрузка данных

Создаем тестовую таблицу и загружаем данные:

```
CREATE TABLE test (
    Region VARCHAR(50),
    Country VARCHAR(50),
    ItemType VARCHAR(50),
    SalesChannel VARCHAR(20),
    OrderPriority VARCHAR(10),
    OrderDate VARCHAR(10),
    OrderID int,
    ShipDate VARCHAR(10),
    UnitsSold int,
    UnitPrice decimal(12,2),
    UnitCost decimal(12,2),
    TotalRevenue decimal(12,2),
    TotalCost decimal(12,2),
    TotalProfit decimal(12,2)
);

root@:26257/test_db> import INTO test (Region,Country,ItemType,SalesChannel,OrderPriority,OrderDate,OrderID,ShipDate,UnitsSold,UnitPrice,UnitCost,TotalRevenue,TotalCost,TotalProfit
) CSV DATA ('gs://postgres13/1000000SalesRecords.csv') WITH DELIMITER = ',', SKIP = '1';
        job_id       |  status   | fraction_completed |  rows   | index_entries |   bytes
---------------------+-----------+--------------------+---------+---------------+------------
  729508532595458049 | succeeded |                  1 | 1000000 |             0 | 134108488
(1 row)

Time: 19.966s total (execution 19.965s / network 0.000s)
```

Проверим скорость работы запроса кол-ва записей:

```
root@:26257/test_db> select count(*) from test;
   count
-----------
  1000000
(1 row)

Time: 428ms total (execution 428ms / network 0ms)
```

#### 7. Настройка посгри

Создаем еще один инстанс для посгри и ставим его туда.

Создаем таблицу:
```
CREATE TABLE test (
    Region VARCHAR(50),
    Country VARCHAR(50),
    ItemType VARCHAR(50),
    SalesChannel VARCHAR(20),
    OrderPriority VARCHAR(10),
    OrderDate VARCHAR(10),
    OrderID int,
    ShipDate VARCHAR(10),
    UnitsSold int,
    UnitPrice decimal(12,2),
    UnitCost decimal(12,2),
    TotalRevenue decimal(12,2),
    TotalCost decimal(12,2),
    TotalProfit decimal(12,2)
);
```

включаем вывод тайминга
```
\timing
```

Файл с датасетом я зарнее скачал в домашную дирректорию юзера.

И заполняем данными:
```
postgres=# COPY test (Region,Country,ItemType,SalesChannel,OrderPriority,OrderDate,OrderID,ShipDate,UnitsSold,UnitPrice,UnitCost,TotalRevenue,TotalCost,TotalProfit)
FROM PROGRAM 'awk FNR-1 /home/hound672/1000000SalesRecordsNEW.csv | cat' DELIMITER ',' CSV HEADER;
COPY 999999
Time: 5197.280 ms (00:05.197)
```

Попробуем посчитать кол-во записей:
```
postgres=# select count(*) from test;
 count
--------
 1000000
(1 row)

Time: 134.231 ms
```

Но по скорости вариант с посгресом оказался как-то быстрее )
