1.
Создал проект на Google Cloud Platform с именем postgres2021-19890604

2.
Дал доступ для пользователя ifti@yandex.ru
IAM -> ADD -> Project Editor

3.
Создал новую ВМ с дефолтным конфигом

4.
Прописал публичную часть ssh ключа в разделе Metadata

5.
Для удобного доступа к ВМ в .ssh/config прописан настройку для новой ВМ
```
Host gce-psql1
    HostName 104.154.96.225
    Port 22
    User hound672
    IdentityFile /home/hound/.ssh/keys/hound/gce
```

6.
Установил postgres 14-ой версии

7,
Открыл вторую ssh сессию.
В обоих сессиях запустил psql под пользователем postgres

8.
Отключил автокоммит

9.
Создал тестовую БД:
```
CREATE DATABASE lesson_1;
```

10.
Переключился на новую БД в обоих сессиях
```
\c lesson_1
```

11.
Создал новую таблицу и наполнил ее тестовым набором данных

12.
Начал по транхации в каждой сессии

13.
Q: видите ли вы новую запись и если да то почему?
A: Нет, т.к при транзакции с уровнем read committed не допускается грязное чтение, а именно чтение данных, которые еще не были зафиксированы (не был выполнен commit).

14.
Сделал коммит в первой сессии

15.
Q: видите ли вы новую запись и если да то почему?
A: Да. т.к мы сделали коммит в первой сессии.

16.
Снова запустил по транзакции в каждом из двух терминалов, но уже с уровнем repeatable read

17.
В первой сессии добавил новую запись.
А во второй сессии выборку по таблице

18.
Q: видите ли вы новую запись и если да то почему?
A: Нет, т.к аналогично с уровнем read committed уровень repeatable read также не допускает аномалию грязного чтения.

19.
Закоммитил транзакцию в первой сессии.
И сделал выборку по таблице во второй сессии.

20.
Q: видите ли вы новую запись и если да то почему?
A: Нет, т.к уровень транзакции repeatable read не допускает аномалию фантомного чтения. В начавшейся транзакции мы видим только тот набор данных, который был доступен нам на момент начала транзакции.

21.
Закоммитил транзакцию во второй сессии.
И снова сделал выборку по таблице.

22.
Q: видите ли вы новую запись и если да то почему?
A: Да, т.к мы завершили транзакцию и "обновили" список доступных записей.

23.
Завершил работу ВМ.