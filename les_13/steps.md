### Триггеры

#### 1. Подготовка

Скачал скрипт, применил его:
1. Создал схему
2. Создал таблицы
3. и т.д

#### 2. Написание триггера

1. Я решил сделать для каждого действия (INSERT, UPDATE, DELETE) по своей отдельной функции и тригеру.
Так мне кажется, что в будующем проще будет отлаживать, чем городить уловия в теле функции по типу:
```
IF action == UPDATE ...
ELIF action == INSERT ...
```

Первая сложность была в том, что когда мы создаем первую продажу, то в таблице ```good_sum_mart``` 
у нас еще нет никакх записей. А значит нам надо сначала проверить существует ли запись с данным товаром.
И если нет, то создать, а если существует, то сделать UPDATE.

Написал такую функцию:

```
CREATE OR REPLACE FUNCTION sales_insert_tgr_function()
RETURNS trigger
AS
$TRIG_FUNC$
DECLARE
    item_name varchar;
    item_price numeric(12, 2);
BEGIN
    SELECT
           goods.good_name, goods.good_price INTO item_name, item_price
    FROM goods WHERE goods.goods_id = NEW.good_id;

    IF EXISTS (SELECT 1 FROM good_sum_mart WHERE good_sum_mart.good_name = item_name)
    THEN
        RAISE NOTICE 'EXISTS';
        UPDATE good_sum_mart SET sum_sale = sum_sale + item_price * NEW.sales_qty WHERE good_name = item_name;
    ELSE
        RAISE NOTICE 'DOES NOT EXISTS';
        INSERT INTO good_sum_mart (good_name, sum_sale) VALUES (item_name, item_price * NEW.sales_qty);
    END IF;

    RETURN NULL; -- триггер у нас AFTER значит наличие значения в return не приницпильно (иначе вернули бы NEW, чтобы строка вставилась)
END;
$TRIG_FUNC$
  LANGUAGE plpgsql
  VOLATILE
    SET search_path = pract_functions, public;
```

Потом создаю сам триггер:
```
CREATE TRIGGER sales_tgr
AFTER INSERT
ON sales
FOR EACH ROW
EXECUTE PROCEDURE sales_insert_tgr_function()
```

Но потом попробовал переписать используя другой подход:
```
CREATE OR REPLACE FUNCTION sales_insert_tgr_function()
RETURNS trigger
AS
$TRIG_FUNC$
DECLARE
    item_name varchar;
    item_price numeric(12, 2);
BEGIN
    SELECT
           goods.good_name, goods.good_price INTO item_name, item_price
    FROM goods WHERE goods.goods_id = NEW.good_id;

    UPDATE good_sum_mart SET sum_sale = sum_sale + item_price * NEW.sales_qty WHERE good_name = item_name;
    IF NOT FOUND THEN
        INSERT INTO good_sum_mart (good_name, sum_sale) VALUES (item_name, item_price * NEW.sales_qty);
    END IF;

    RETURN NULL; -- триггер у нас AFTER значит наличие значения в return не приницпильно (иначе вернули бы NEW, чтобы строка вставилась)
END;
$TRIG_FUNC$
  LANGUAGE plpgsql
  VOLATILE
    SET search_path = pract_functions, public;
```

Тут мы не проверяем, что запись такая существует, а сразу пытаемся ее обновить.
Об успешном обновлении говорит значение переменной ```FOUND```, и если мы ничего не обновили, то значит
записи нет и мы ее создаем.

Но потом я подумал, что можно еще сделать по другому (чуть ниже).

2. Триггер на обновление

Функция:
```
CREATE OR REPLACE FUNCTION sales_update_tgr_function()
RETURNS trigger
AS
$TRIG_FUNC$
DECLARE
    item_name varchar;
    item_price numeric(12, 2);
    price_delta numeric(12, 2);
BEGIN
    SELECT
           goods.good_name, goods.good_price INTO item_name, item_price
    FROM goods WHERE goods.goods_id = NEW.good_id;

    price_delta := (NEW.sales_qty * item_price) - (OLD.sales_qty * item_price);
    UPDATE good_sum_mart SET sum_sale = sum_sale + price_delta WHERE good_name = item_name;

    RETURN NULL; -- триггер у нас AFTER значит наличие значения в return не приницпильно
END;
$TRIG_FUNC$
  LANGUAGE plpgsql
  VOLATILE
    SET search_path = pract_functions, public;
```

Создание триггера:
```
CREATE TRIGGER sales_update_tgr
AFTER UPDATE
ON sales
FOR EACH ROW
EXECUTE PROCEDURE sales_update_tgr_function();
```

Тут я не делаю проверку существует запись или нет, т.к если такой продажи нет, то триггер не вызовится.
В самой функции я просто считаю дельту в итоговой цене, которая получается после апдейта, а она может быть
и минусовая в случае уменьшения кол-ва проданого товара.

3. Триггер на удаление

```
CREATE OR REPLACE FUNCTION sales_delete_tgr_function()
RETURNS trigger
AS
$TRIG_FUNC$
DECLARE
    item_name varchar;
    item_price numeric(12, 2);
BEGIN
    SELECT
           goods.good_name, goods.good_price INTO item_name, item_price
    FROM goods WHERE goods.goods_id = OLD.good_id;
    UPDATE good_sum_mart SET sum_sale = sum_sale - item_price * OLD.sales_qty WHERE good_name = item_name;
    RETURN NULL; -- триггер у нас AFTER значит наличие значения в return не приницпильно
END;
$TRIG_FUNC$
  LANGUAGE plpgsql
  VOLATILE
    SET search_path = pract_functions, public;
```

И создание триггера:
```
CREATE TRIGGER sales_delete_tgr
AFTER DELETE
ON sales
FOR EACH ROW
EXECUTE PROCEDURE sales_delete_tgr_function();
```

#### 3. Альтернативный вариант

Т.к таблица ```good_sum_mart``` должна содержать товары в единственном экземпляре, то можно сделать проверку на уникальность значения по этому полю:
```
ALTER TABLE good_sum_mart ADD CONSTRAINT unique_good_name UNIQUE (good_name);
```

Теперь мы не сможем вставить дублирующее имя товара, а значит можем переписать ф-цию триггера на INSERT на следующую:
```
CREATE OR REPLACE FUNCTION sales_insert_tgr_function()
RETURNS trigger
AS
$TRIG_FUNC$
DECLARE
    item_name varchar;
    item_price numeric(12, 2);
BEGIN
    SELECT
           goods.good_name, goods.good_price INTO item_name, item_price
    FROM goods WHERE goods.goods_id = NEW.good_id;

    INSERT INTO good_sum_mart (good_name, sum_sale) VALUES (item_name, item_price * NEW.sales_qty)
    ON CONFLICT (good_name) DO UPDATE SET sum_sale = excluded.sum_sale + good_sum_mart.sum_sale;

    RETURN NULL; -- триггер у нас AFTER значит наличие значения в return не приницпильно (иначе вернули бы NEW, чтобы строка вставилась)
END;
$TRIG_FUNC$
  LANGUAGE plpgsql
  VOLATILE
    SET search_path = pract_functions, public;
```

Тут мы используем конструкцию ```ON CONFLICT DO UPDATE```, которая вызывается в том случае если у нас был нарушена уникальность записи при вставке.

#### 4. Чем такая схема (витрина+триггер) предпочтительнее отчета, создаваемого "по требованию" (кроме производительности)?

Если у нас поменяется цена за товар (таблица ```goods```), то при текущей схеме цена в отчетной таблице не пересчитается,
для этого нужно будет создавать еще триггер для таблици ```goods```, а для отчета "по требования" ничего такого делать не придется,
т.к вся информация будет пересчитываться. Но это минус для триггера скорее)

Мне данная ситуация с триггерами кажется, что может принести опреденные проблемы из-за своей не очевидности,
если данная фича не документирована на проекте. А логировать через ```RAISE NOTICE``` как-то лишний раз засорять логи.
Как-то можно посмотреть информацию о том, что триггеры вызывались?

Я сталкивался с подобной денормализацией таблицы для ответов в случае ```event storming``` когда была похожая таблица для отчетов
и в которую какой-то фоновый процесс "по крону" записывал актуальные данные из нормальзованных таблиц, как раз для ускорения построения отчета.
Но там это делалось не триггерами)

Честно говоря, кроме прозводительности особых плюсов не вижу.

-----

Вопросы:
1. Как лучше дебажить такие штуки(функции, процедуры, триггеры)? Можно ли кроме отладки "принтами" что-то другое использовать? Какие-то отладчики, например?

2. У меня в коде ф-ций всегда встречается похожее:
```
    SELECT
           goods.good_name, goods.good_price INTO item_name, item_price
    FROM goods WHERE goods.goods_id = NEW.good_id;
```
Кажется, что ничего такого, но это дублирование и потенциально можно с этим поймать проблем, когда мы по какой-то причине поменяем именя полей, например.
Я думал обернуть такое в выделеную ф-цию, но в любом случае придется писать ```INTO``` чтобы записать значение в переменную.
Хотя с выделеной ф-цией мы все равно можно обезопасить себя от возможных изменения в схеме таблицы.
Или можно как-то по другом в данном кейсе записать значение в переменную?

3. Вариант с ```ON CONFLICT``` мне нравится больше, т.к кроме прочего еще и принудительно добавляет проверку на уникальность. Но что тут будет более производительнее?

4. Как-то возможно профилировать ф-ции, процедуры. С запросами все понятно, там есть ```EXPLAIN```, а тут я ничего такого не нашел :(
