### Индексы

#### 1. Создадим простую таблицу:

```
CREATE TABLE simple_table (
    id serial,
    value int
);
```

#### 2. Заполняем данными

```
INSERT INTO simple_table (value) SELECT generate_series AS value FROM generate_series(100001, 1000000);
```

#### 3. Делаем выборку

Сначала попробуем найти что-то без создания индекса:

```
EXPLAIN ANALYZE
SELECT * FROM simple_table WHERE value = 512345;
```

Смотрим план запроса:
```
Gather  (cost=1000.00..9670.60 rows=1 width=8) (actual time=98.538..141.531 rows=1 loops=1)
  Workers Planned: 2
  Workers Launched: 2
  ->  Parallel Seq Scan on simple_table  (cost=0.00..8670.50 rows=1 width=8) (actual time=8.230..15.743 rows=0 loops=3)
        Filter: (value = 512345)
        Rows Removed by Filter: 300000
Planning Time: 0.096 ms
Execution Time: 159.559 ms
```

```Seq Scan``` говорит о том, что у нас был последоватльный перебор данных в таблице, что логично т.к индекса нет
и СУДБ необходимо пройтись по каждому элементу в таблице.

Строим индекс по полю ```value```:

```
CREATE INDEX ON simple_table(value);
```

Имя индекса отсавляем по дефолту, тип тоже (btree нам как раз и нужен).

И пробуем снова запрос:
```
EXPLAIN ANALYZE
SELECT * FROM simple_table WHERE value = 512345;
```

Результат:
```
Index Scan using simple_table_value_idx on simple_table  (cost=0.42..8.44 rows=1 width=8) (actual time=0.024..0.026 rows=1 loops=1)
  Index Cond: (value = 512345)
Planning Time: 0.118 ms
Execution Time: 0.045 ms
```

Время запроса сильно выросло, и теперь мы использовали индекс для поиск, вместо перебора всех данных.
Но в данном случае мы запросили ысе данные из таблицы (```SELECT *```), теперь попроуем выбирать только
поле ```value```.

```
EXPLAIN ANALYZE
SELECT value FROM simple_table WHERE value = 512345;
```

Получаем:
```
Index Only Scan using simple_table_value_idx on simple_table  (cost=0.42..4.44 rows=1 width=4) (actual time=0.019..0.021 rows=1 loops=1)
  Index Cond: (value = 512345)
  Heap Fetches: 0
Planning Time: 0.080 ms
Execution Time: 0.037 ms
```

Время выполнения запроса еще раз сократилось и теперь получили ```Index Only Scan``` вместо ```Index Scan using```
Что говорит о том, после того как с помощью индекса мы нашли указатель на нужную строку в таблице
мы не стаали считывать данные из таблицы, а воспольозвались только значением, которое было записано в индексе.

#### 4. Полнотекстовый поиск

Созданием другую таблицу, с спец. типом, на который можем повесить индекс ```GIN``` для полнотекстового поиска:

```
CREATE TABLE full_text_search (
    id serial,
    text_raw text,
    text_lexeme tsvector
)
```

Наполняем данными:
```
INSERT INTO full_text_search(text_raw, text_lexeme)
SELECT generate.concat_ws AS text_raw, to_tsvector(generate.concat_ws) AS text_lexeme
FROM (SELECT concat_ws(' ', (array ['go', 'space', 'sun', 'London'])[(random() * 5)::int]
                    , (array ['the', 'capital', 'of', 'Great', 'Britain'])[(random() * 6)::int]
                    , (array ['some', 'another', 'example', 'with', 'words'])[(random() * 6)::int]
                    ) FROM generate_series(100001, 1000000)) generate
```

Тут у нас 2 колонки, с исходным текстом и дублирующем его содержимое, но с типом ```tsvector``` чтобы можно было
построить индекс для полнотекстого поиска.

Т.е на клиент мы выводим данные из колонки ```text_raw```, а для поиска используем ```text_lexeme```

!!! ВОПРОС: такой подход вызывает вопросы на счет расходования ресурсов.
Хранить подобные дубликаты.
Мне кажется, что тут может быть хорошей альтернативой ElasticSearch, подобные вещи, как большие текста и поиск по ним
делать в другой СУБД.
На сколько верные данные утверждения?

Теперь без индекса пробуем что-то поискать:
```
EXPLAIN ANALYSE
SELECT *
FROM full_text_search
WHERE full_text_search.text_lexeme @@ to_tsquery('britains');
```

Получаем план запроса:
```
Gather  (cost=1000.00..123092.50 rows=149490 width=47) (actual time=3.213..416.520 rows=150273 loops=1)
  Workers Planned: 2
  Workers Launched: 2
  ->  Parallel Seq Scan on full_text_search  (cost=0.00..107143.50 rows=62288 width=47) (actual time=2.693..382.799 rows=50091 loops=3)
        Filter: (text_lexeme @@ to_tsquery('britains'::text))
        Rows Removed by Filter: 249909
Planning Time: 0.101 ms
JIT:
  Functions: 6
"  Options: Inlining false, Optimization false, Expressions true, Deforming true"
"  Timing: Generation 1.094 ms, Inlining 0.000 ms, Optimization 0.569 ms, Emission 6.643 ms, Total 8.306 ms"
Execution Time: 421.024 ms
```

```Seq Scan``` без индекса снова используем перебор всех данных в таблице.
Созданием индекс:

```
CREATE INDEX full_text_search_idx ON full_text_search USING gin (text_lexeme);
```

Повторяем прошлый запрос и получаем следующий план запроса:
```
Gather  (cost=2382.80..42388.27 rows=149490 width=47) (actual time=14.193..47.060 rows=150273 loops=1)
  Workers Planned: 2
  Workers Launched: 2
  ->  Parallel Bitmap Heap Scan on full_text_search  (cost=1382.80..26439.27 rows=62288 width=47) (actual time=4.673..14.708 rows=50091 loops=3)
        Recheck Cond: (text_lexeme @@ to_tsquery('britains'::text))
        Heap Blocks: exact=2511
        ->  Bitmap Index Scan on full_text_search_idx  (cost=0.00..1345.42 rows=149490 width=0) (actual time=12.944..12.944 rows=150273 loops=1)
              Index Cond: (text_lexeme @@ to_tsquery('britains'::text))
Planning Time: 22.769 ms
Execution Time: 50.892 ms
```

Уже используем индекс.
Попробуем повторить "трюк" из прошлого пункта, когда запрашивали не все данные, а только то поле по которому 
у нас построен индекс:

```
EXPLAIN ANALYSE
SELECT text_lexeme
FROM full_text_search
WHERE full_text_search.text_lexeme @@ to_tsquery('britains');
```

План запроса:
```
Gather  (cost=2382.80..42388.27 rows=149490 width=29) (actual time=13.835..48.034 rows=150273 loops=1)
  Workers Planned: 2
  Workers Launched: 2
  ->  Parallel Bitmap Heap Scan on full_text_search  (cost=1382.80..26439.27 rows=62288 width=29) (actual time=4.566..17.330 rows=50091 loops=3)
        Recheck Cond: (text_lexeme @@ to_tsquery('britains'::text))
        Heap Blocks: exact=2389
        ->  Bitmap Index Scan on full_text_search_idx  (cost=0.00..1345.42 rows=149490 width=0) (actual time=12.451..12.451 rows=150273 loops=1)
              Index Cond: (text_lexeme @@ to_tsquery('britains'::text))
Planning Time: 0.124 ms
Execution Time: 52.067 ms
```

Но что-то ничего не поменялось)
Мы не смогли уместить все данные в этой колнке в индекс?

#### 5. Реализовать индекс на часть таблицы

Сделаем еще одну таблицу:

```
CREATE TABLE part_index (
    id serial,
    field bool
)
```

Строим индекс по полю ```field```:

```
CREATE INDEX ON part_index(field) WHERE field = true;
```

Наполняем данными, важно что мы тут строим индекс только на часть таблицы.
Например, мы заранее значем, что значий ```false``` у нас будет больше, а значит нет смысл их искать с помощью
индекса, в отличие от значений ```true```, которых меньше и для которых индекс может сильно помочь.

```
INSERT INTO part_index (field)
SELECT random() < 0.01 FROM generate_series(1,100000)
```

Делаем выборку по значению ```false```

```
EXPLAIN ANALYSE
SELECT * FROM part_index WHERE field = false
```

Получаем пдан запроса:
```
Seq Scan on part_index  (cost=0.00..1537.21 rows=54710 width=5) (actual time=0.015..14.465 rows=99049 loops=1)
  Filter: (NOT field)
  Rows Removed by Filter: 951
Planning Time: 0.114 ms
Execution Time: 18.509 ms
```

Как и ожидалось для значений ```false``` индекс не используется, теперь пробем выборку для значений ```true```:

```
EXPLAIN ANALYSE
SELECT * FROM part_index WHERE field = true

Index Scan using part_index_field_idx on part_index  (cost=0.15..102.91 rows=973 width=5) (actual time=0.008..0.601 rows=951 loops=1)
Planning Time: 0.174 ms
Execution Time: 0.660 ms
```

И ожидаемо используем индекс.

#### 6. Создать индекс на несколько полей

Создаем таблицу:
```
CREATE TABLE several_idx (
    id serial,
    val1 int,
    val2 int
)
```

Строим индекс:
```
CREATE INDEX ON several_idx(val1, val2)
```

Заполняем данными:

```
INSERT INTO several_idx (val1, val2)
SELECT generate_series AS val1, generate_series AS val2
FROM generate_series(100001, 1000000);
```

Пробуем запрос:
```
Bitmap Heap Scan on several_idx  (cost=4.68..99.68 rows=25 width=12) (actual time=0.012..0.013 rows=0 loops=1)
  Recheck Cond: ((val1 = 222222) AND (val2 = 1111111))
  ->  Bitmap Index Scan on several_idx_val1_val2_idx  (cost=0.00..4.67 rows=25 width=0) (actual time=0.010..0.010 rows=0 loops=1)
        Index Cond: ((val1 = 222222) AND (val2 = 1111111))
Planning Time: 0.114 ms
Execution Time: 0.029 ms
```

Как и ожидалось используется индекс.

---

### Соединения

#### 1. Подготовка

Создадим 3 таблицы:

```
CREATE TABLE users (
    id serial PRIMARY KEY ,
    name varchar
)

CREATE TABLE orders (
    id serial PRIMARY KEY ,
    user_id int references users
)

CREATE TABLE items (
    id serial,
    order_id int references orders,
    title varchar
```

Наполняем их данными:

```
INSERT INTO users (name) VALUES ('john doe'), ('mick smith'), ('empty user')
INSERT INTO orders (id, user_id) VALUES (1, 1), (2, 1), (3, 2), (4, NULL);
INSERT INTO items (order_id, title) VALUES (1, 'tea'), (1, 'bread'), (3, 'oil')
```

#### 2. Реализовать прямое соединение двух или более таблиц

Посмотрим список товаров, которые были куплены пользователями, причем в выборке пусть будут только имя пользователя
и имя товара
```
SELECT users.name, items.title FROM users
JOIN orders ON users.id = orders.user_id
JOIN items ON orders.id = items.order_id

name title
john doe,tea
john doe,bread
mick smith,oil
```

#### 3. Реализовать левостороннее (или правостороннее) соединение двух или более таблиц

Выведем всех пользователей с заказами и без.

```
SELECT users.name FROM users
LEFT JOIN orders ON orders.user_id = users.id
GROUP BY users.name
```

#### 4. Реализовать кросс соединение двух или более таблиц

Перемножим вообще все данные всех таблиц)

```
SELECT * FROM users
CROSS JOIN orders
CROSS JOIN items

1,john doe,1,1,4,1,tea
1,john doe,2,1,4,1,tea
1,john doe,3,2,4,1,tea
1,john doe,1,1,5,1,bread
1,john doe,2,1,5,1,bread
1,john doe,3,2,5,1,bread
1,john doe,1,1,6,3,oil
1,john doe,2,1,6,3,oil
1,john doe,3,2,6,3,oil
2,mick smith,1,1,4,1,tea
2,mick smith,2,1,4,1,tea
2,mick smith,3,2,4,1,tea
2,mick smith,1,1,5,1,bread
2,mick smith,2,1,5,1,bread
2,mick smith,3,2,5,1,bread
2,mick smith,1,1,6,3,oil
2,mick smith,2,1,6,3,oil
2,mick smith,3,2,6,3,oil
3,empty user,1,1,4,1,tea
3,empty user,2,1,4,1,tea
3,empty user,3,2,4,1,tea
3,empty user,1,1,5,1,bread
3,empty user,2,1,5,1,bread
3,empty user,3,2,5,1,bread
3,empty user,1,1,6,3,oil
3,empty user,2,1,6,3,oil
3,empty user,3,2,6,3,oil
```

#### 5. Реализовать полное соединение двух или более таблиц

Выведем все заказы и всех пользователей.
```
SELECT * FROM users
FULL JOIN orders ON users.id = orders.user_id;
```

#### 6. Реализовать запрос, в котором будут использованы разные типы соединений

```
SELECT * FROM users
JOIN orders ON users.id = orders.user_id
LEFT JOIN items ON orders.id = items.order_id
```
