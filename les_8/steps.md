#### 1. сделать инстанс Google Cloud Engine типа e2-medium с ОС Ubuntu 20.04

done

#### 2. поставить на него PostgreSQL 13 из пакетов собираемых postgres.org

done

#### 3. нагрузить кластер через утилиту
https://github.com/Percona-Lab/sysbench-tpcc (требует установки
https://github.com/akopytov/sysbench)

Утилиту поставил согласно доки,
скрипты для бенча склонировал на ВМ.

Перед запуском первого скрипта по подготовке БД, поставил большой таймаут для чекпоинтов,
предпологая, что так быстрее может работать запись в БД.

Запустил подготовоку командой
```
./tpcc.lua --pgsql-host=127.0.0.1 --pgsql-port=5432 --pgsql-user=postgres --pgsql-password=12345 --pgsql-db=sbt --time=300 --threads=8 --report-interval=1 --tables=10 --scale=100 --db-driver=pgsql prepare
```

Уменьшил кол-во потоков для запуска утилиты.
И ждем...

После того как срипт отработал запускаю сам бенч.
Пока не меняю настройки посгреса.
```
./tpcc.lua --pgsql-host=127.0.0.1 --pgsql-port=5432 --pgsql-user=postgres --pgsql-password=12345 --pgsql-db=sbt --time=300 --threads=8 --report-interval=1 --tables=10 --scale=100 --db-driver=pgsql run
```

Получил следующие результаты:
```
 ] thds: 16 tps: 144.61 qps: 4113.82 (r/w/o: 1836.01/1894.85/382.96) lat (ms,95%): 262.64 err/s 22.94 reconn/s: 0.00
[ 2s ] thds: 16 tps: 174.11 qps: 5045.14 (r/w/o: 2291.43/2354.47/399.25) lat (ms,95%): 227.40 err/s 27.02 reconn/s: 0.00
[ 3s ] thds: 16 tps: 187.99 qps: 5367.74 (r/w/o: 2423.88/2525.88/417.98) lat (ms,95%): 207.82 err/s 21.00 reconn/s: 0.00
[ 4s ] thds: 16 tps: 189.01 qps: 5590.18 (r/w/o: 2542.08/2625.08/423.01) lat (ms,95%): 204.11 err/s 24.00 reconn/s: 0.00
[ 5s ] thds: 16 tps: 195.00 qps: 5749.96 (r/w/o: 2613.98/2718.98/417.00) lat (ms,95%): 189.93 err/s 16.00 reconn/s: 0.00
[ 6s ] thds: 16 tps: 209.99 qps: 5932.78 (r/w/o: 2698.90/2768.90/464.98) lat (ms,95%): 170.48 err/s 23.00 reconn/s: 0.00
[ 7s ] thds: 16 tps: 188.01 qps: 5616.31 (r/w/o: 2541.14/2663.15/412.02) lat (ms,95%): 219.36 err/s 18.00 reconn/s: 0.00
[ 8s ] thds: 16 tps: 145.00 qps: 4675.92 (r/w/o: 2120.96/2226.96/327.99) lat (ms,95%): 350.33 err/s 20.00 reconn/s: 0.00
[ 9s ] thds: 16 tps: 102.00 qps: 3711.14 (r/w/o: 1698.06/1777.07/236.01) lat (ms,95%): 502.20 err/s 16.00 reconn/s: 0.00
[ 10s ] thds: 16 tps: 97.00 qps: 2974.93 (r/w/o: 1348.97/1397.97/227.99) lat (ms,95%): 484.44 err/s 19.00 reconn/s: 0.00
[ 11s ] thds: 16 tps: 133.99 qps: 4386.55 (r/w/o: 2023.79/2066.79/295.97) lat (ms,95%): 411.96 err/s 17.00 reconn/s: 0.00
[ 12s ] thds: 16 tps: 113.99 qps: 3536.81 (r/w/o: 1619.91/1662.91/253.99) lat (ms,95%): 520.62 err/s 13.00 reconn/s: 0.00
[ 13s ] thds: 16 tps: 115.01 qps: 3433.43 (r/w/o: 1538.19/1617.20/278.03) lat (ms,95%): 467.30 err/s 24.00 reconn/s: 0.00
[ 14s ] thds: 16 tps: 131.99 qps: 4180.74 (r/w/o: 1898.88/1967.88/313.98) lat (ms,95%): 539.71 err/s 28.00 reconn/s: 0.00
[ 15s ] thds: 16 tps: 139.02 qps: 3909.49 (r/w/o: 1792.22/1813.23/304.04) lat (ms,95%): 277.21 err/s 13.00 reconn/s: 0.00
[ 16s ] thds: 16 tps: 149.00 qps: 4301.92 (r/w/o: 1927.97/2009.96/363.99) lat (ms,95%): 325.98 err/s 33.00 reconn/s: 0.00
[ 17s ] thds: 16 tps: 153.99 qps: 4336.78 (r/w/o: 1962.90/2007.90/365.98) lat (ms,95%): 331.91 err/s 29.00 reconn/s: 0.00
[ 18s ] thds: 16 tps: 155.00 qps: 4684.02 (r/w/o: 2127.01/2187.01/370.00) lat (ms,95%): 292.60 err/s 33.00 reconn/s: 0.00
[ 19s ] thds: 16 tps: 163.00 qps: 4661.11 (r/w/o: 2123.05/2170.05/368.01) lat (ms,95%): 325.98 err/s 24.00 reconn/s: 0.00
[ 20s ] thds: 16 tps: 169.97 qps: 5043.16 (r/w/o: 2283.62/2353.61/405.93) lat (ms,95%): 320.17 err/s 34.99 reconn/s: 0.00
[ 21s ] thds: 16 tps: 153.02 qps: 4983.54 (r/w/o: 2254.24/2375.26/354.04) lat (ms,95%): 308.84 err/s 25.00 reconn/s: 0.00
[ 22s ] thds: 16 tps: 166.02 qps: 5076.62 (r/w/o: 2307.28/2399.29/370.05) lat (ms,95%): 356.70 err/s 22.00 reconn/s: 0.00
[ 23s ] thds: 16 tps: 157.98 qps: 4894.46 (r/w/o: 2227.75/2296.74/369.96) lat (ms,95%): 369.77 err/s 27.00 reconn/s: 0.00
[ 24s ] thds: 16 tps: 131.98 qps: 4263.50 (r/w/o: 1923.78/2031.76/307.96) lat (ms,95%): 344.08 err/s 23.00 reconn/s: 0.00
[ 25s ] thds: 16 tps: 163.02 qps: 5231.64 (r/w/o: 2366.29/2453.30/412.05) lat (ms,95%): 363.18 err/s 44.01 reconn/s: 0.00
[ 26s ] thds: 16 tps: 192.01 qps: 5661.17 (r/w/o: 2579.08/2658.08/424.01) lat (ms,95%): 227.40 err/s 23.00 reconn/s: 0.00
[ 27s ] thds: 16 tps: 175.00 qps: 5295.98 (r/w/o: 2400.99/2500.99/394.00) lat (ms,95%): 253.35 err/s 22.00 reconn/s: 0.00
[ 28s ] thds: 16 tps: 130.00 qps: 3603.95 (r/w/o: 1623.98/1663.98/316.00) lat (ms,95%): 467.30 err/s 28.00 reconn/s: 0.00
[ 29s ] thds: 16 tps: 114.00 qps: 3083.96 (r/w/o: 1387.98/1433.98/262.00) lat (ms,95%): 331.91 err/s 18.00 reconn/s: 0.00
[ 30s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 31s ] thds: 16 tps: 8.00 qps: 337.99 (r/w/o: 154.00/168.00/16.00) lat (ms,95%): 2279.14 err/s 0.00 reconn/s: 0.00
[ 32s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 33s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 34s ] thds: 16 tps: 18.00 qps: 616.00 (r/w/o: 277.00/299.00/40.00) lat (ms,95%): 5507.54 err/s 2.00 reconn/s: 0.00
[ 35s ] thds: 16 tps: 54.01 qps: 1726.17 (r/w/o: 782.08/808.08/136.01) lat (ms,95%): 909.80 err/s 14.00 reconn/s: 0.00
[ 36s ] thds: 16 tps: 27.00 qps: 740.95 (r/w/o: 341.98/342.98/56.00) lat (ms,95%): 6835.96 err/s 1.00 reconn/s: 0.00
[ 37s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 38s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 39s ] thds: 16 tps: 0.00 qps: 1.00 (r/w/o: 1.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 40s ] thds: 16 tps: 5.00 qps: 170.00 (r/w/o: 75.00/85.00/10.00) lat (ms,95%): 3841.98 err/s 0.00 reconn/s: 0.00
[ 41s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 42s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 43s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 44s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 45s ] thds: 16 tps: 17.00 qps: 354.04 (r/w/o: 154.02/162.02/38.00) lat (ms,95%): 9624.59 err/s 2.00 reconn/s: 0.00
[ 46s ] thds: 16 tps: 45.00 qps: 1536.12 (r/w/o: 695.05/737.06/104.01) lat (ms,95%): 10343.39 err/s 8.00 reconn/s: 0.00
[ 47s ] thds: 16 tps: 35.99 qps: 1403.77 (r/w/o: 639.90/683.89/79.99) lat (ms,95%): 511.33 err/s 4.00 reconn/s: 0.00
[ 48s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 49s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 50s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 51s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 52s ] thds: 16 tps: 35.01 qps: 897.18 (r/w/o: 391.08/420.08/86.02) lat (ms,95%): 5709.50 err/s 9.00 reconn/s: 0.00
[ 53s ] thds: 16 tps: 64.00 qps: 2258.09 (r/w/o: 1048.04/1068.04/142.01) lat (ms,95%): 511.33 err/s 6.00 reconn/s: 0.00
[ 54s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 55s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 56s ] thds: 16 tps: 31.00 qps: 805.88 (r/w/o: 358.95/380.95/65.99) lat (ms,95%): 3151.62 err/s 2.00 reconn/s: 0.00
[ 57s ] thds: 16 tps: 0.00 qps: 7.00 (r/w/o: 7.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 58s ] thds: 16 tps: 23.00 qps: 622.13 (r/w/o: 276.06/290.06/56.01) lat (ms,95%): 2493.86 err/s 5.00 reconn/s: 0.00
[ 59s ] thds: 16 tps: 4.00 qps: 87.00 (r/w/o: 39.00/38.00/10.00) lat (ms,95%): 2985.89 err/s 1.00 reconn/s: 0.00
[ 60s ] thds: 16 tps: 5.00 qps: 175.00 (r/w/o: 76.00/77.00/22.00) lat (ms,95%): 4280.32 err/s 6.00 reconn/s: 0.00
[ 61s ] thds: 16 tps: 19.00 qps: 595.04 (r/w/o: 277.02/280.02/38.00) lat (ms,95%): 2198.52 err/s 0.00 reconn/s: 0.00
[ 62s ] thds: 16 tps: 10.00 qps: 186.00 (r/w/o: 82.00/84.00/20.00) lat (ms,95%): 4128.91 err/s 0.00 reconn/s: 0.00
[ 63s ] thds: 16 tps: 28.99 qps: 1019.80 (r/w/o: 466.91/482.91/69.99) lat (ms,95%): 6594.16 err/s 6.00 reconn/s: 0.00
[ 64s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 65s ] thds: 16 tps: 15.00 qps: 426.94 (r/w/o: 180.98/213.97/32.00) lat (ms,95%): 2539.17 err/s 1.00 reconn/s: 0.00
[ 66s ] thds: 16 tps: 0.00 qps: 1.00 (r/w/o: 1.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 67s ] thds: 16 tps: 27.00 qps: 676.03 (r/w/o: 306.01/312.01/58.00) lat (ms,95%): 3982.86 err/s 2.00 reconn/s: 0.00
[ 68s ] thds: 16 tps: 29.99 qps: 748.77 (r/w/o: 339.89/336.90/71.98) lat (ms,95%): 2985.89 err/s 6.00 reconn/s: 0.00
[ 69s ] thds: 16 tps: 81.02 qps: 2076.39 (r/w/o: 932.17/958.18/186.03) lat (ms,95%): 1401.61 err/s 12.00 reconn/s: 0.00
[ 70s ] thds: 16 tps: 18.00 qps: 473.03 (r/w/o: 214.01/217.01/42.00) lat (ms,95%): 1013.60 err/s 3.00 reconn/s: 0.00
[ 71s ] thds: 16 tps: 57.00 qps: 1769.02 (r/w/o: 795.01/836.01/138.00) lat (ms,95%): 1453.01 err/s 12.00 reconn/s: 0.00
[ 72s ] thds: 16 tps: 20.00 qps: 736.98 (r/w/o: 336.99/357.99/42.00) lat (ms,95%): 1973.38 err/s 1.00 reconn/s: 0.00
[ 73s ] thds: 16 tps: 21.00 qps: 609.01 (r/w/o: 278.00/287.00/44.00) lat (ms,95%): 1089.30 err/s 1.00 reconn/s: 0.00
[ 74s ] thds: 16 tps: 21.00 qps: 668.99 (r/w/o: 302.00/319.00/48.00) lat (ms,95%): 1069.86 err/s 3.00 reconn/s: 0.00
[ 75s ] thds: 16 tps: 27.99 qps: 823.76 (r/w/o: 376.89/386.89/59.98) lat (ms,95%): 1771.29 err/s 2.00 reconn/s: 0.00
[ 76s ] thds: 16 tps: 38.01 qps: 1051.31 (r/w/o: 469.14/484.14/98.03) lat (ms,95%): 2198.52 err/s 11.00 reconn/s: 0.00
[ 77s ] thds: 16 tps: 22.00 qps: 615.00 (r/w/o: 277.00/292.00/46.00) lat (ms,95%): 1013.60 err/s 2.00 reconn/s: 0.00
[ 78s ] thds: 16 tps: 29.00 qps: 955.94 (r/w/o: 426.97/450.97/78.00) lat (ms,95%): 3095.38 err/s 10.00 reconn/s: 0.00
[ 79s ] thds: 16 tps: 24.00 qps: 626.04 (r/w/o: 278.02/298.02/50.00) lat (ms,95%): 2045.74 err/s 1.00 reconn/s: 0.00
[ 80s ] thds: 16 tps: 30.00 qps: 914.01 (r/w/o: 412.01/436.01/66.00) lat (ms,95%): 1213.57 err/s 3.00 reconn/s: 0.00
[ 81s ] thds: 16 tps: 35.99 qps: 1188.66 (r/w/o: 540.84/551.84/95.97) lat (ms,95%): 733.00 err/s 13.00 reconn/s: 0.00
[ 82s ] thds: 16 tps: 40.00 qps: 1016.06 (r/w/o: 456.03/470.03/90.01) lat (ms,95%): 1561.52 err/s 5.00 reconn/s: 0.00
[ 83s ] thds: 16 tps: 10.00 qps: 499.09 (r/w/o: 231.04/242.04/26.00) lat (ms,95%): 2493.86 err/s 3.00 reconn/s: 0.00
[ 84s ] thds: 16 tps: 0.00 qps: 2.00 (r/w/o: 2.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 85s ] thds: 16 tps: 36.01 qps: 962.19 (r/w/o: 423.08/463.09/76.01) lat (ms,95%): 3386.99 err/s 2.00 reconn/s: 0.00
[ 86s ] thds: 16 tps: 35.99 qps: 1086.79 (r/w/o: 488.91/505.90/91.98) lat (ms,95%): 1258.08 err/s 10.00 reconn/s: 0.00
[ 87s ] thds: 16 tps: 0.00 qps: 0.00 (r/w/o: 0.00/0.00/0.00) lat (ms,95%): 0.00 err/s 0.00 reconn/s: 0.00
[ 88s ] thds: 16 tps: 67.00 qps: 2061.11 (r/w/o: 942.05/969.05/150.01) lat (ms,95%): 1771.29 err/s 8.00 reconn/s: 0.00
[ 89s ] thds: 16 tps: 37.00 qps: 910.97 (r/w/o: 408.99/421.99/80.00) lat (ms,95%): 926.33 err/s 3.00 reconn/s: 0.00
[ 90s ] thds: 16 tps: 30.00 qps: 880.98 (r/w/o: 396.99/409.99/74.00) lat (ms,95%): 2405.65 err/s 7.00 reconn/s: 0.00
[ 91s ] thds: 16 tps: 1.00 qps: 12.00 (r/w/o: 10.00/0.00/2.00) lat (ms,95%): 179.94 err/s 0.00 reconn/s: 0.00
[ 92s ] thds: 16 tps: 35.99 qps: 1159.81 (r/w/o: 520.91/564.91/73.99) lat (ms,95%): 2539.17 err/s 1.00 reconn/s: 0.00
[ 93s ] thds: 16 tps: 50.01 qps: 1790.21 (r/w/o: 829.10/841.10/120.01) lat (ms,95%): 1149.76 err/s 10.00 reconn/s: 0.00
[ 94s ] thds: 16 tps: 59.00 qps: 1635.06 (r/w/o: 741.03/760.03/134.00) lat (ms,95%): 816.63 err/s 8.00 reconn/s: 0.00
[ 95s ] thds: 16 tps: 41.00 qps: 1199.07 (r/w/o: 543.03/560.03/96.01) lat (ms,95%): 759.88 err/s 7.00 reconn/s: 0.00
[ 96s ] thds: 16 tps: 47.00 qps: 1338.03 (r/w/o: 602.01/628.02/108.00) lat (ms,95%): 2932.60 err/s 8.00 reconn/s: 0.00
[ 97s ] thds: 16 tps: 13.00 qps: 382.00 (r/w/o: 171.00/179.00/32.00) lat (ms,95%): 2632.28 err/s 3.00 reconn/s: 0.00
[ 98s ] thds: 16 tps: 30.00 qps: 1132.94 (r/w/o: 526.97/539.97/66.00) lat (ms,95%): 1739.68 err/s 3.00 reconn/s: 0.00
[ 99s ] thds: 16 tps: 16.00 qps: 501.87 (r/w/o: 226.94/236.94/37.99) lat (ms,95%): 1938.16 err/s 3.00 reconn/s: 0.00
[ 100s ] thds: 16 tps: 50.01 qps: 1435.37 (r/w/o: 652.17/667.17/116.03) lat (ms,95%): 1533.66 err/s 8.00 reconn/s: 0.00
```

Теперь попробую тюнить конфиг посгри. Для начала обращусь к ресурсу ```https://pgtune.leopard.in.ua/#/```

У меня получились следующие параметры:
```
max_connections = 20
shared_buffers = 1GB
effective_cache_size = 3GB
maintenance_work_mem = 256MB
checkpoint_completion_target = 0.9
wal_buffers = 16MB
default_statistics_target = 100
random_page_cost = 4
effective_io_concurrency = 2
work_mem = 52428kB
min_wal_size = 1GB
max_wal_size = 4GB
max_worker_processes = 2
max_parallel_workers_per_gather = 1
max_parallel_workers = 2
max_parallel_maintenance_workers = 1
```

Которые я прописал в конфиг файл посгреса, после чего перезапустил сервис.

И повторно запустил бенч.

И получил уже такие результаты:
```
[ 1s ] thds: 16 tps: 149.45 qps: 4618.99 (r/w/o: 2087.31/2161.04/370.64) lat (ms,95%): 227.40 err/s 11.96 reconn/s: 0.00
[ 2s ] thds: 16 tps: 160.30 qps: 4435.32 (r/w/o: 1991.74/2086.91/356.67) lat (ms,95%): 253.35 err/s 19.04 reconn/s: 0.00
[ 3s ] thds: 16 tps: 157.99 qps: 4911.77 (r/w/o: 2221.90/2329.89/359.98) lat (ms,95%): 211.60 err/s 22.00 reconn/s: 0.00
[ 4s ] thds: 16 tps: 199.01 qps: 5260.27 (r/w/o: 2368.12/2458.12/434.02) lat (ms,95%): 207.82 err/s 21.00 reconn/s: 0.00
[ 5s ] thds: 16 tps: 165.00 qps: 5196.93 (r/w/o: 2355.97/2470.97/369.99) lat (ms,95%): 211.60 err/s 21.00 reconn/s: 0.00
[ 6s ] thds: 16 tps: 176.00 qps: 5289.96 (r/w/o: 2402.98/2486.98/400.00) lat (ms,95%): 200.47 err/s 24.00 reconn/s: 0.00
[ 7s ] thds: 16 tps: 180.01 qps: 5535.17 (r/w/o: 2509.07/2628.08/398.01) lat (ms,95%): 193.38 err/s 20.00 reconn/s: 0.00
[ 8s ] thds: 16 tps: 171.00 qps: 5529.94 (r/w/o: 2517.97/2631.97/380.00) lat (ms,95%): 204.11 err/s 19.00 reconn/s: 0.00
[ 9s ] thds: 16 tps: 183.98 qps: 5450.52 (r/w/o: 2483.78/2560.77/405.96) lat (ms,95%): 196.89 err/s 20.00 reconn/s: 0.00
[ 10s ] thds: 16 tps: 192.98 qps: 5276.43 (r/w/o: 2394.74/2445.74/435.95) lat (ms,95%): 200.47 err/s 28.00 reconn/s: 0.00
[ 11s ] thds: 16 tps: 208.04 qps: 5587.04 (r/w/o: 2535.47/2593.48/458.08) lat (ms,95%): 193.38 err/s 21.00 reconn/s: 0.00
[ 12s ] thds: 16 tps: 183.00 qps: 5627.03 (r/w/o: 2571.02/2658.02/398.00) lat (ms,95%): 215.44 err/s 16.00 reconn/s: 0.00
[ 13s ] thds: 16 tps: 204.00 qps: 5623.06 (r/w/o: 2550.03/2619.03/454.00) lat (ms,95%): 223.34 err/s 23.00 reconn/s: 0.00
[ 14s ] thds: 16 tps: 198.99 qps: 5750.85 (r/w/o: 2610.93/2679.93/459.99) lat (ms,95%): 189.93 err/s 32.00 reconn/s: 0.00
[ 15s ] thds: 16 tps: 222.99 qps: 5926.66 (r/w/o: 2689.84/2716.84/519.97) lat (ms,95%): 196.89 err/s 37.00 reconn/s: 0.00
[ 16s ] thds: 16 tps: 203.01 qps: 6078.20 (r/w/o: 2754.09/2870.09/454.01) lat (ms,95%): 189.93 err/s 26.00 reconn/s: 0.00
[ 17s ] thds: 16 tps: 199.00 qps: 6034.91 (r/w/o: 2743.96/2850.96/439.99) lat (ms,95%): 189.93 err/s 22.00 reconn/s: 0.00
[ 18s ] thds: 16 tps: 215.01 qps: 6077.38 (r/w/o: 2743.17/2848.18/486.03) lat (ms,95%): 179.94 err/s 29.00 reconn/s: 0.00
[ 19s ] thds: 16 tps: 204.00 qps: 6045.90 (r/w/o: 2746.95/2829.95/468.99) lat (ms,95%): 183.21 err/s 31.00 reconn/s: 0.00
[ 20s ] thds: 16 tps: 210.01 qps: 5816.15 (r/w/o: 2665.07/2692.07/459.01) lat (ms,95%): 196.89 err/s 20.00 reconn/s: 0.00
[ 21s ] thds: 16 tps: 210.00 qps: 6043.94 (r/w/o: 2743.97/2823.97/476.00) lat (ms,95%): 196.89 err/s 28.00 reconn/s: 0.00
[ 22s ] thds: 16 tps: 220.96 qps: 6255.79 (r/w/o: 2841.45/2914.44/499.90) lat (ms,95%): 183.21 err/s 30.99 reconn/s: 0.00
[ 23s ] thds: 16 tps: 222.04 qps: 6043.00 (r/w/o: 2746.45/2816.46/480.08) lat (ms,95%): 200.47 err/s 19.00 reconn/s: 0.00
[ 24s ] thds: 16 tps: 213.01 qps: 6126.19 (r/w/o: 2774.09/2876.09/476.02) lat (ms,95%): 215.44 err/s 25.00 reconn/s: 0.00
[ 25s ] thds: 16 tps: 217.99 qps: 6347.80 (r/w/o: 2895.91/2964.91/486.98) lat (ms,95%): 186.54 err/s 26.00 reconn/s: 0.00
[ 26s ] thds: 16 tps: 213.00 qps: 6360.13 (r/w/o: 2892.06/2980.06/488.01) lat (ms,95%): 189.93 err/s 33.00 reconn/s: 0.00
[ 27s ] thds: 16 tps: 210.00 qps: 6199.09 (r/w/o: 2810.04/2927.04/462.01) lat (ms,95%): 200.47 err/s 21.00 reconn/s: 0.00
[ 28s ] thds: 16 tps: 205.99 qps: 6375.64 (r/w/o: 2903.84/3022.83/448.97) lat (ms,95%): 186.54 err/s 19.00 reconn/s: 0.00
[ 29s ] thds: 16 tps: 214.99 qps: 6444.58 (r/w/o: 2929.81/3046.80/467.97) lat (ms,95%): 173.58 err/s 19.00 reconn/s: 0.00
[ 30s ] thds: 16 tps: 226.03 qps: 6571.80 (r/w/o: 2984.36/3077.37/510.06) lat (ms,95%): 173.58 err/s 29.00 reconn/s: 0.00
[ 31s ] thds: 16 tps: 240.00 qps: 6677.97 (r/w/o: 3011.99/3135.99/530.00) lat (ms,95%): 170.48 err/s 27.00 reconn/s: 0.00
[ 32s ] thds: 16 tps: 223.00 qps: 6585.86 (r/w/o: 2999.94/3091.94/493.99) lat (ms,95%): 179.94 err/s 24.00 reconn/s: 0.00
[ 33s ] thds: 16 tps: 230.01 qps: 6584.21 (r/w/o: 2974.10/3078.10/532.02) lat (ms,95%): 164.45 err/s 38.00 reconn/s: 0.00
[ 34s ] thds: 16 tps: 233.00 qps: 6437.97 (r/w/o: 2925.99/2994.99/517.00) lat (ms,95%): 161.51 err/s 26.00 reconn/s: 0.00
[ 35s ] thds: 16 tps: 213.00 qps: 6437.99 (r/w/o: 2933.00/3022.00/483.00) lat (ms,95%): 173.58 err/s 31.00 reconn/s: 0.00
[ 36s ] thds: 16 tps: 220.99 qps: 6566.80 (r/w/o: 2982.91/3075.90/507.98) lat (ms,95%): 167.44 err/s 33.00 reconn/s: 0.00
[ 37s ] thds: 16 tps: 221.97 qps: 6343.08 (r/w/o: 2872.58/2956.57/513.93) lat (ms,95%): 183.21 err/s 36.99 reconn/s: 0.00
[ 38s ] thds: 16 tps: 210.00 qps: 6371.14 (r/w/o: 2914.06/2991.06/466.01) lat (ms,95%): 183.21 err/s 24.00 reconn/s: 0.00
[ 39s ] thds: 16 tps: 213.01 qps: 6221.41 (r/w/o: 2825.19/2925.19/471.03) lat (ms,95%): 176.73 err/s 23.00 reconn/s: 0.00
[ 40s ] thds: 16 tps: 209.02 qps: 6551.58 (r/w/o: 2964.26/3122.27/465.04) lat (ms,95%): 173.58 err/s 23.00 reconn/s: 0.00
[ 41s ] thds: 16 tps: 224.00 qps: 6628.00 (r/w/o: 3000.00/3104.00/524.00) lat (ms,95%): 176.73 err/s 39.00 reconn/s: 0.00
[ 42s ] thds: 16 tps: 219.99 qps: 6604.80 (r/w/o: 2994.91/3121.91/487.99) lat (ms,95%): 173.58 err/s 24.00 reconn/s: 0.00
[ 43s ] thds: 16 tps: 223.98 qps: 6590.51 (r/w/o: 3004.78/3093.77/491.96) lat (ms,95%): 167.44 err/s 22.00 reconn/s: 0.00
[ 44s ] thds: 16 tps: 220.02 qps: 6361.65 (r/w/o: 2861.29/3004.31/496.05) lat (ms,95%): 189.93 err/s 28.00 reconn/s: 0.00
[ 45s ] thds: 16 tps: 212.00 qps: 6624.02 (r/w/o: 2983.01/3158.01/483.00) lat (ms,95%): 179.94 err/s 31.00 reconn/s: 0.00
[ 46s ] thds: 16 tps: 227.00 qps: 6483.91 (r/w/o: 2924.96/3052.96/505.99) lat (ms,95%): 179.94 err/s 26.00 reconn/s: 0.00
[ 47s ] thds: 16 tps: 231.98 qps: 6526.32 (r/w/o: 2965.69/3047.68/512.95) lat (ms,95%): 173.58 err/s 26.00 reconn/s: 0.00
[ 48s ] thds: 16 tps: 238.03 qps: 6477.79 (r/w/o: 2933.36/3013.37/531.06) lat (ms,95%): 173.58 err/s 29.00 reconn/s: 0.00
[ 49s ] thds: 16 tps: 228.00 qps: 6483.96 (r/w/o: 2950.98/3031.98/501.00) lat (ms,95%): 179.94 err/s 23.00 reconn/s: 0.00
[ 50s ] thds: 16 tps: 211.00 qps: 6478.92 (r/w/o: 2949.96/3057.96/470.99) lat (ms,95%): 204.11 err/s 27.00 reconn/s: 0.00
[ 51s ] thds: 16 tps: 225.98 qps: 6318.52 (r/w/o: 2857.78/2961.78/498.96) lat (ms,95%): 189.93 err/s 25.00 reconn/s: 0.00
[ 52s ] thds: 16 tps: 216.02 qps: 6827.57 (r/w/o: 3135.26/3216.27/476.04) lat (ms,95%): 176.73 err/s 24.00 reconn/s: 0.00
[ 53s ] thds: 16 tps: 205.00 qps: 6604.86 (r/w/o: 3012.94/3127.93/463.99) lat (ms,95%): 170.48 err/s 28.00 reconn/s: 0.00
[ 54s ] thds: 16 tps: 212.01 qps: 6389.26 (r/w/o: 2904.12/3011.12/474.02) lat (ms,95%): 173.58 err/s 25.00 reconn/s: 0.00
[ 55s ] thds: 16 tps: 219.99 qps: 6162.77 (r/w/o: 2780.90/2886.89/494.98) lat (ms,95%): 183.21 err/s 28.00 reconn/s: 0.00
[ 56s ] thds: 16 tps: 225.00 qps: 6175.12 (r/w/o: 2787.05/2889.05/499.01) lat (ms,95%): 179.94 err/s 24.00 reconn/s: 0.00
[ 57s ] thds: 16 tps: 219.98 qps: 6593.42 (r/w/o: 2991.74/3115.73/485.96) lat (ms,95%): 179.94 err/s 24.00 reconn/s: 0.00
[ 58s ] thds: 16 tps: 212.01 qps: 6236.27 (r/w/o: 2834.12/2927.13/475.02) lat (ms,95%): 193.38 err/s 26.00 reconn/s: 0.00
[ 59s ] thds: 16 tps: 185.94 qps: 6039.02 (r/w/o: 2766.09/2868.06/404.87) lat (ms,95%): 189.93 err/s 15.99 reconn/s: 0.00
[ 60s ] thds: 16 tps: 231.06 qps: 6149.62 (r/w/o: 2785.73/2826.74/537.14) lat (ms,95%): 176.73 err/s 38.01 reconn/s: 0.00
[ 61s ] thds: 16 tps: 230.01 qps: 6600.28 (r/w/o: 2992.13/3085.13/523.02) lat (ms,95%): 183.21 err/s 31.00 reconn/s: 0.00
[ 62s ] thds: 16 tps: 218.01 qps: 6565.39 (r/w/o: 2985.18/3104.19/476.03) lat (ms,95%): 173.58 err/s 20.00 reconn/s: 0.00
[ 63s ] thds: 16 tps: 218.00 qps: 6858.11 (r/w/o: 3117.05/3253.05/488.01) lat (ms,95%): 170.48 err/s 27.00 reconn/s: 0.00
[ 64s ] thds: 16 tps: 231.00 qps: 6727.93 (r/w/o: 3070.97/3122.97/533.99) lat (ms,95%): 161.51 err/s 37.00 reconn/s: 0.00
[ 65s ] thds: 16 tps: 232.00 qps: 6649.11 (r/w/o: 3025.05/3111.05/513.01) lat (ms,95%): 170.48 err/s 28.00 reconn/s: 0.00
[ 66s ] thds: 16 tps: 211.99 qps: 6553.73 (r/w/o: 2967.88/3101.87/483.98) lat (ms,95%): 186.54 err/s 32.00 reconn/s: 0.00
[ 67s ] thds: 16 tps: 222.00 qps: 6750.05 (r/w/o: 3080.02/3177.03/493.00) lat (ms,95%): 170.48 err/s 24.00 reconn/s: 0.00
[ 68s ] thds: 16 tps: 249.00 qps: 6752.07 (r/w/o: 3050.03/3137.03/565.01) lat (ms,95%): 161.51 err/s 35.00 reconn/s: 0.00
[ 69s ] thds: 16 tps: 212.00 qps: 6890.02 (r/w/o: 3150.01/3247.01/493.00) lat (ms,95%): 170.48 err/s 34.00 reconn/s: 0.00
[ 70s ] thds: 16 tps: 219.00 qps: 6470.06 (r/w/o: 2930.03/3043.03/497.00) lat (ms,95%): 179.94 err/s 30.00 reconn/s: 0.00
[ 71s ] thds: 16 tps: 234.99 qps: 6759.78 (r/w/o: 3062.90/3163.90/532.98) lat (ms,95%): 155.80 err/s 33.00 reconn/s: 0.00
[ 72s ] thds: 16 tps: 223.00 qps: 6549.93 (r/w/o: 2967.97/3071.97/509.99) lat (ms,95%): 161.51 err/s 32.00 reconn/s: 0.00
[ 73s ] thds: 16 tps: 224.01 qps: 6636.30 (r/w/o: 3006.13/3128.14/502.02) lat (ms,95%): 176.73 err/s 29.00 reconn/s: 0.00
[ 74s ] thds: 16 tps: 221.00 qps: 6804.98 (r/w/o: 3105.99/3186.99/512.00) lat (ms,95%): 173.58 err/s 36.00 reconn/s: 0.00
[ 75s ] thds: 16 tps: 223.99 qps: 6467.60 (r/w/o: 2938.82/3034.81/493.97) lat (ms,95%): 170.48 err/s 23.00 reconn/s: 0.00
[ 76s ] thds: 16 tps: 219.01 qps: 6471.19 (r/w/o: 2929.09/3057.09/485.01) lat (ms,95%): 176.73 err/s 24.00 reconn/s: 0.00
[ 77s ] thds: 16 tps: 225.01 qps: 6451.25 (r/w/o: 2911.11/3025.12/515.02) lat (ms,95%): 170.48 err/s 33.00 reconn/s: 0.00
[ 78s ] thds: 16 tps: 225.00 qps: 6495.93 (r/w/o: 2946.97/3046.97/501.99) lat (ms,95%): 170.48 err/s 26.00 reconn/s: 0.00
[ 79s ] thds: 16 tps: 213.00 qps: 6274.00 (r/w/o: 2852.00/2948.00/474.00) lat (ms,95%): 183.21 err/s 25.00 reconn/s: 0.00
[ 80s ] thds: 16 tps: 220.00 qps: 6314.98 (r/w/o: 2871.99/2964.99/478.00) lat (ms,95%): 183.21 err/s 19.00 reconn/s: 0.00
[ 81s ] thds: 16 tps: 235.00 qps: 6682.05 (r/w/o: 3054.02/3116.03/512.00) lat (ms,95%): 161.51 err/s 21.00 reconn/s: 0.00
[ 82s ] thds: 16 tps: 218.00 qps: 6604.04 (r/w/o: 3003.02/3129.02/472.00) lat (ms,95%): 176.73 err/s 20.00 reconn/s: 0.00
[ 83s ] thds: 16 tps: 210.00 qps: 6707.94 (r/w/o: 3049.97/3183.97/474.00) lat (ms,95%): 179.94 err/s 28.00 reconn/s: 0.00
[ 84s ] thds: 16 tps: 231.98 qps: 6707.54 (r/w/o: 3056.79/3141.78/508.97) lat (ms,95%): 164.45 err/s 25.00 reconn/s: 0.00
[ 85s ] thds: 16 tps: 207.01 qps: 6645.25 (r/w/o: 3048.12/3144.12/453.02) lat (ms,95%): 186.54 err/s 19.00 reconn/s: 0.00
[ 86s ] thds: 16 tps: 220.99 qps: 6547.72 (r/w/o: 2975.87/3061.87/509.98) lat (ms,95%): 173.58 err/s 33.00 reconn/s: 0.00
[ 87s ] thds: 16 tps: 209.01 qps: 6473.18 (r/w/o: 2964.08/3037.08/472.01) lat (ms,95%): 183.21 err/s 28.00 reconn/s: 0.00
[ 88s ] thds: 16 tps: 214.99 qps: 6266.75 (r/w/o: 2828.89/2955.88/481.98) lat (ms,95%): 186.54 err/s 27.00 reconn/s: 0.00
[ 89s ] thds: 16 tps: 222.02 qps: 6440.44 (r/w/o: 2927.20/3019.20/494.03) lat (ms,95%): 173.58 err/s 26.00 reconn/s: 0.00
[ 90s ] thds: 16 tps: 215.96 qps: 6490.81 (r/w/o: 2942.46/3052.44/495.91) lat (ms,95%): 173.58 err/s 33.99 reconn/s: 0.00
[ 91s ] thds: 16 tps: 224.05 qps: 6319.29 (r/w/o: 2875.59/2943.60/500.10) lat (ms,95%): 193.38 err/s 28.01 reconn/s: 0.00
[ 92s ] thds: 16 tps: 228.00 qps: 6719.86 (r/w/o: 3065.93/3153.93/499.99) lat (ms,95%): 170.48 err/s 25.00 reconn/s: 0.00
[ 93s ] thds: 16 tps: 213.00 qps: 6640.06 (r/w/o: 3012.03/3140.03/488.00) lat (ms,95%): 179.94 err/s 31.00 reconn/s: 0.00
[ 94s ] thds: 16 tps: 210.00 qps: 6606.04 (r/w/o: 3014.02/3128.02/464.00) lat (ms,95%): 167.44 err/s 26.00 reconn/s: 0.00
[ 95s ] thds: 16 tps: 231.00 qps: 6656.97 (r/w/o: 3026.99/3109.99/520.00) lat (ms,95%): 179.94 err/s 29.00 reconn/s: 0.00
[ 96s ] thds: 16 tps: 221.00 qps: 6568.12 (r/w/o: 2988.05/3098.06/482.01) lat (ms,95%): 173.58 err/s 20.00 reconn/s: 0.00
[ 97s ] thds: 16 tps: 235.00 qps: 7008.86 (r/w/o: 3189.94/3290.93/527.99) lat (ms,95%): 161.51 err/s 33.00 reconn/s: 0.00
[ 98s ] thds: 16 tps: 227.00 qps: 6451.02 (r/w/o: 2933.01/3014.01/504.00) lat (ms,95%): 170.48 err/s 26.00 reconn/s: 0.00
[ 99s ] thds: 16 tps: 209.00 qps: 6475.95 (r/w/o: 2954.98/3052.98/468.00) lat (ms,95%): 176.73 err/s 25.00 reconn/s: 0.00
[ 100s ] thds: 16 tps: 232.00 qps: 6667.88 (r/w/o: 3028.95/3120.94/517.99) lat (ms,95%): 176.73 err/s 29.00 reconn/s: 0.00
```

В среднем tps вырос.

Ради эксперемента решил вернуть дефолтные настройки и снова прогнать бенч.

Показатели вернулись к первым значениям (стало хуже чем было после тюнинга конфига).
Откат к дефолтным настройкам я сделал чтобы посмотреть мог ли как-то бенч показать другие результаты из-за деградации базы, например.

Вернул параметры к тем, которые были созданы с помощью ресурса ```https://pgtune.leopard.in.ua/#/```
