1. Создал ВМ на GCE.

2. Установил Docker

3. Создал директорию ```/var/lib/postgres```

4. Открыл порт ```5432``` с помощью вэб интерфейса GCE.

5. Создал ```docker-compose.yml``` файл
```
version: '3'

services:
  postgres:
    image: postgres:13
    container_name: postgres
    environment:
      POSTGRES_DB: demo
      POSTGRES_USER: user
      POSTGRES_PASSWORD: secret
    volumes:
      - /var/lib/postgres:/var/lib/postgresql/data
    ports:
      - '5432:5432'

```

6. Запустил контейнер ```docker-compose up -d```

7. помощью ```docker ps``` проверил, что контекнер запустился

8. Подключился клиентом из докера 
```docker exec -it postgres psql -U user -d demo```

9. Создал таблицу и добавил в нее пару записей.

10. Подключился с локального ПК к базе:
```psql -h 35.192.113.241 -U user -d demo```

11. Проверил, что удалось подключится к БД и что доступна ранее созданная таблица и записи в ней.

12. Останавливаю контейнер
```docker stop postgres```
P.S Можно и через ```docker-compose``` находясь в директории с ```docker-compose.yml``` файлом:
```docker-compose down```

13. Удаляю контейнер
```docker rm postgres```

14. Проверил, что образ удален
```docker ps -a```

15. Создал заново контейнер
```
docker-compose up d
```

16. Подключился по аналогии из прошлых пунктов к БД и проверил, что все данные на месте.
