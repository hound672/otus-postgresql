#### 1. Создаем 4 ВМ

Все стандартно, как ранее создаем через консоль 4 ВМ, ставим на каждую посгрес 14.


#### 2. Настроим pg_hba.

Нам нужен будет доступ с одной машины на другую, поэтому надо:
1. Поставить пароль для юзера, который будет работать с СУДБ - пусть это будет дефолтный postgres
2. Включить внешний доступ в pg_hba

На машины будем "ходить" используя внутрений айпи, у меня они получись такие:
```
(ВМ 1) 10.128.0.12
(ВМ 2) 10.128.0.13
(ВМ 3) 10.128.0.14
(ВМ 4) 10.128.0.15
```

Попробум подключиться из ВМ 1 на ВМ2

```
psql -h 10.128.0.13 -U postgres
```

Получаем ошибку:
```
psql: error: connection to server at "10.128.0.13", port 5432 failed: Connection refused
```

Для этого надо настроить ```pg_hba.conf```. Идем на ВМ 2 и прописываем в конец файла:
```
sudo vim /etc/postgresql/14/main/pg_hba.conf
```

```
host    all             all              0.0.0.0/0              md5
```

И сделать изменнения в конфиге посгреса
```
sudo vim /etc/postgresql/14/main/postgresql.conf
```

Находим строку ```listen_addresses``` и меняем на ```listen_addresses = '*'```

Перезапускаем кластер
```
sudo pg_ctlcluster 14 main restart
```

Можно посмотреть через netstat, что сейчас посгрес может принимать внешние подключения.

Пробуем снова подключиться с ВМ 1.
Теперь коннект проходит, но просит пароль...

Заходим в psql на ВМ2 и ставим пароль:
```
sudo -u postgres psql
\password
```

И вводим пароль.

После чего мы уже смогли успешно подключиться.

Производим все подобные манипуляции для всех остальных ВМ.

---
!!!ВОПРОС!!!: Что касается разрешения подключения с любых хостов.
Я в своей практике делал так:
1. Открывал в настройках СУБД внешние коннекты
2. С помощью файрвола (iptables/ufw) запрещал доступ к хост машине с СУДБ для всех коннектов, кроме тех кому это нужно (бекенды)

На сколько такой подход имеет место быть и/или что лучше использовать?

---

#### 3. Настроим репликацию на ВМ 1 и ВМ 2

На ВМ 1 и ВМ 2 создаем 2 таблицы:
```
CREATE TABLE test (id serial, value int);
CREATE TABLE test2 (id serial, value int);
```

На обоих хостах меняем настройки
```
ALTER SYSTEM SET wal_level = logical;
```

И перезапускаем кластер
```
sudo pg_ctlcluster 14 main restart
```

И проверяем, что параметр применился после перезапуска кластера:
```
show wal_level;
 wal_level
-----------
 logical
(1 row)
```

На обоих серверах режим сменился.

На ВМ 1 создаем публикацию для таблицы ```test```

```
CREATE PUBLICATION test_pub FOR TABLE test;
```

А на ВМ 2 публикацию для таблицы ```test2```

```
CREATE PUBLICATION test2_pub FOR TABLE test2;
```

Проверяем, что публикация создалась: ```\dRp+```

Создаем подписку на ВМ 1:
```
CREATE SUBSCRIPTION test2_sub
CONNECTION 'host=10.128.0.13 port=5432 user=postgres password=12345 dbname=postgres' 
PUBLICATION test2_pub WITH (copy_data = true);
```

Проверим, что все ОК:
```
postgres=# \dRs
            List of subscriptions
   Name   |  Owner   | Enabled | Publication
----------+----------+---------+-------------
 test2_sub | postgres | t       | {test2_pub}
(1 row)
```

Провернем похожее на ВМ 2
```
CREATE SUBSCRIPTION test_sub
CONNECTION 'host=10.128.0.12 port=5432 user=postgres password=12345 dbname=postgres' 
PUBLICATION test_pub WITH (copy_data = true);
```

Вставками данных провряем, что данные синхрлнизируются
При вставке в таболицу ```test``` нв ВМ 1 мы видим новые данные на ВМ 1 в таблице ```test```.

#### 4. Сделать получение данных на ВМ 3

Создаем 2 таблицы:
```
CREATE TABLE test (id serial, value int);
CREATE TABLE test2 (id serial, value int);
```

И создаем подписки:
```
CREATE SUBSCRIPTION vm_3_test2_sub
CONNECTION 'host=10.128.0.13 port=5432 user=postgres password=12345 dbname=postgres'
PUBLICATION test2_pub WITH (copy_data = true);
```

и

```
CREATE SUBSCRIPTION vm_3_test_sub
CONNECTION 'host=10.128.0.12 port=5432 user=postgres password=12345 dbname=postgres'
PUBLICATION test_pub WITH (copy_data = true);
```

Проверяем, что все реплика работает
Сначала селект по таблицам, а потом вставкой.

#### 5. Теперь приступим к настройке физической репликации.

План такой:
ВМ 3 подписывается на изменения таблиц с ВМ 1 и ВМ 2 и является мастером, слейвом будет ВМ 4.

На ВМ 3 созданидим еще одну тестовую таблицу.

```
CREATE TABLE test3 (id serial, value int);
INSERT INTO test3 VALUES (1, 11);
```

Проверим уровень wal:
```
show wal_level;
```

Текущий уровень ```replica``` - значит все ок, идем дальше.

На ВМ 4 стопаем кластер
```
sudo pg_ctlcluster 14 main stop
```

Проверяем, что кластер остановился:
```
hound672@instance-4:~$ pg_lsclusters
Ver Cluster Port Status Owner    Data directory              Log file
14  main    5432 down   postgres /var/lib/postgresql/14/main /var/log/postgresql/postgresql-14-main.log
```

Удаляем данные кластера:
```
sudo rm -rf /var/lib/postgresql/14/main
```

И сделаем бекап бд из ВМ 3
```
sudo -u postgres pg_basebackup -h 10.128.0.14 -p 5432 -R -D /var/lib/postgresql/14/main
```

Но получили ошибку:
```
pg_basebackup: error: connection to server at "10.128.0.14", port 5432 failed: FATAL:  no pg_hba.conf entry for replication connection from host "10.128.0.15", user "postgres", SSL encryption
connection to server at "10.128.0.14", port 5432 failed: FATAL:  no pg_hba.conf entry for replication connection from host "10.128.0.15", user "postgres", no encryption
```

Идем в pg_hba.conf на ВМ 3 и прописываем следующее:
```
host    replication     all             0.0.0.0/0                 md5
```

После чего перезапускаем кластер и повторяем операцию с бекапом на ВМ 4.

Команда успешно выполнилась.

Проверяем содержимое дирректори с даннными кластера:
```
$ ls /var/lib/postgresql/14
< main
```

Данные появились.

Добавим параметр горячего резерва, чтобы реплика принимала запросы на чтение
```
sudo su posstgres
echo 'hot_standby = on' >> /var/lib/postgresql/14/main/postgresql.auto.conf
```

И запускам кластер
```
sudo pg_ctlcluster 14 main start
```

Проверяем, что кластер запустился:
```
hound672@instance-4:~$ pg_lsclusters
Ver Cluster Port Status Owner    Data directory              Log file
14  main    5432 online postgres /var/lib/postgresql/14/main /var/log/postgresql/postgresql-14-main.log
```

Все ок, кластер запустился. Зайдем теперь в psql и посмотрим какие там сейчас есть таблицы:
```
sudo -u postgres psql
postgres=# \d
              List of relations
 Schema |     Name     |   Type   |  Owner
--------+--------------+----------+----------
 public | test         | table    | postgres
 public | test2        | table    | postgres
 public | test2_id_seq | sequence | postgres
 public | test3        | table    | postgres
 public | test3_id_seq | sequence | postgres
 public | test_id_seq  | sequence | postgres
(6 rows)
```

Проверим содежимое таблиц:
```
postgres=# select * from test;
 id | value
----+-------
  1 |    11
  2 |    22
  3 |    33
  4 |    44
  5 |    55
  6 |    66
 11 |   111
 22 |   222
(8 rows)

postgres=# select * from test2;
 id | value
----+-------
  1 |    11
  2 |    22
  3 |    33
  4 |    44
  5 |    55
  6 |    66
(6 rows)

postgres=# select * from test3;
 id | value
----+-------
  1 |    11
(1 row)
```

Как видно все данные мы видим, те которые были на ВМ 3.

Попробуем что-то выставить в таблицу ```test3``` - мы ожидаем получить ошибку.
```
postgres=# insert into test3 values (2,22);
ERROR:  cannot execute INSERT in a read-only transaction
```

Справедливо получили ошибку.

Теперь проверим работает ли реплика сделаем вставку в таблицу ```test3``` на ВМ 1
и посмотрим отобразятся ли эти данные на ВМ 4.

На ВМ 3 выполняем:
```
postgres=# insert into test3 values (2,22);
INSERT 0 1
```

Данные вставили, теперь идем на ВМ 4 и делаем селект по этой таблице.
```
postgres=# select * from test3;
 id | value
----+-------
  1 |    11
  2 |    22
(2 rows)
```

УРА! Данные видим.
Теперь посмотрим как будет реботать синхронизация с ВМ 1 и ВМ 2.
Для этого на ВМ 1 выполняем:
```
insert into test values (111,111);
```

А на ВМ 2 выполняем:
```
insert into test2 values (222,222);
```

Идем на ВМ 3 и проверяем добавились ли туда данные:
```
postgres=# select * from test where id = 111;
 id  | value
-----+-------
 111 |   111
(1 row)

postgres=# select * from test2 where id = 222;
 id  | value
-----+-------
 222 |   222
(1 row)
```

Все ОК, новые данные мы увидели, теперь проверим тоже самое на ВМ 4.
Смысл в том, что ВМ 3 подписался на ВМ 1 и ВМ 2, получив оттуда изменения ВМ 3 отправляет измененые данные на ВМ 4.

```
postgres=# select * from test where id = 111;
 id  | value
-----+-------
 111 |   111
(1 row)

postgres=# select * from test2 where id = 222;
 id  | value
-----+-------
 222 |   222
(1 row)
```

Ну и на ВМ 4 тоже все.
Считаем, что реплику мы настроили!

#### 6. Сделаем "рокеровку"

Допустим, у нас что-то случилось с ВМ 3, который сейчас мастер.
У нас есть "горячая" реплика, которую мы можем перенастроить на работу в режима "мастер".

Стопаем кластер на ВМ 3
```
sudo pg_ctlcluster 14 main stop
```

На ВМ 4 у нас сразу отвалилась релпикапция:
```
postgres=# select * from pg_stat_wal_receiver \gx
(0 rows)
```

Переводим ВМ 4 в режим мастер:
```
sudo pg_ctlcluster 14 main promote
```

Заходим в psql и попробуем сделать вставку в таблицу ```test3``` ранее у нас была ошибка ппри вставке в эту таблицу
```

postgres=# select * from pg_stat_wal_receiver \gx
stgres=# insert into test3 values (3,33);
INSERT 0 1
postgres=# select * from test3;
 id | value
----+-------
  1 |    11
  2 |    22
  3 |    33
(3 rows)

(0 rows)
```

Данные успешно вставились.
Но теперь надо проверить, что подписка на таблицы из ВМ 1 и ВМ 2 сохранилась.
Идем на ВМ 1 и ВМ 2 и вставляем данные:

ВМ 1
```
insert into test values (1111. 1111);
```

ВМ 2
```
insert into test2 values (2222, 2222);
```

Проверим отобразились ли изменения на ВМ 4.
```


stgres=# select * from test where id = 1111;
  id  | value
------+-------
 1111 |  1111
(1 row)

postgres=# select * from test2 where id = 2222;
  id  | value
------+-------
 2222 |  2222
(1 row)

postgres=# select * from pg_stat_wal_receiver \gx
stgres=# insert into test3 values (3,33);
INSERT 0 1
postgres=# select * from test3;
 id | value
----+-------
  1 |    11
  2 |    22
  3 |    33
(3 rows)

(0 rows)
```

Супер! Подписка сохранилась.

#### 6. Возвращаем реплику

У нас сейчас нет реплики, там реплика которая была (ВМ 4) сейчас мастер, а бывший мастер (ВМ 3) простаивает.

Надо сделать ВМ 3 репликой.

Повтораяем пункты, как мы настраивали ВМ 4.

Проверяем что кластер на ВМ 3 остановлен
```
hound672@instance-3:~$ sudo pg_lsclusters
Ver Cluster Port Status Owner    Data directory              Log file
14  main    5432 down   postgres /var/lib/postgresql/14/main /var/log/postgresql/postgresql-14-main.log
```

Удаляем данные кластера
```
sudo rm -rf /var/lib/postgresql/14/main
```

Делаем бекап
```
sudo -u postgres pg_basebackup -h 10.128.0.15 -p 5432 -R -D /var/lib/postgresql/14/main
```

Как и в прошлый раз получили ошибку...идем в pg_hba на ВМ 4

И снова повторяем копирование.

На этот раз все выполнилось без ошибки.

Добавим параметр горячего резерва, чтобы реплика принимала запросы на чтение
```
sudo su posstgres
echo 'hot_standby = on' >> /var/lib/postgresql/14/main/postgresql.auto.conf
```

И запускаем кластер
```
sudo pg_ctlcluster 14 main start
hund672@instance-3:~$ sudo pg_lsclusters
Ver Cluster Port Status          Owner    Data directory              Log file
14  main    5432 online,recovery postgres /var/lib/postgresql/14/main /var/log/postgresql/postgresql-14-main.log
```

Заходим в psql и пробуем прочитать данные
```
stgres=# select * from test;
  id  | value
------+-------
    1 |    11
    2 |    22
    3 |    33
    4 |    44
    5 |    55
    6 |    66
   11 |   111
   22 |   222
  111 |   111
 1111 |  1111
(10 rows)

postgres=# select * from test2;
  id  | value
------+-------
    1 |    11
    2 |    22
    3 |    33
    4 |    44
    5 |    55
    6 |    66
  222 |   222
 2222 |  2222
(8 rows)

postgres=# select * from test3;
 id | value
----+-------
  1 |    11
  2 |    22
  3 |    33
(3 rows)
```

Все данные на месте.

Проверим ошибку записи данных:
```
postgres=# insert into test3 values (1,1);
ERROR:  cannot execute INSERT in a read-only transaction

```

Тоже все ок.

Проверим синхронизацию с ВМ 1, ВМ 2 и ВМ 4.

ВМ 1
```
insert into test values (1111,1111);
```

ВМ 2
```
insert into test2 values (2222,2222);
```

ВМ 4
```
insert into test3 values (4444,4444);
```

Идем на ВМ 3 и проверяем, что новые данные мы там видим.
```
stgres=# select * from test where id = 1111;
id  | value
------+-------
1111 |  1111
1111 |  1111
(2 rows)

postgres=# select * from test2 where id = 2222;
id  | value
------+-------
2222 |  2222
(1 row)

postgres=# select * from test3 where id = 4444;
id  | value
------+-------
4444 |  4444
(1 row)
```

Все новые данные видим.

Новая реплика тоже работает!
